/*
 * 負責各個查詢頁面存取資料表的工作，
 如：進入志工基本資料查詢頁面，要顯示對應的志工基本資料列，
 按下查看，要記錄被選取的列的志工身分證字號，
 按下刪除，要呼叫EJB的刪除方法。
 */
package com.vol.bean;

import com.my.Ejb.OrgEJBLocal;
import com.my.Ejb.RegisterEJBLocal;
import com.my.Ejb.ServiceHoursEJBLocal;
import com.my.Ejb.TrainingEJBLocal;
import com.my.Ejb.UserEJBLocal;
import com.my.Ejb.VolunteerBeanLocal;
import com.my.Entity.BasicInformationEntity;
import com.my.Entity.EducationTraining;
import com.my.Entity.Org;
import com.my.Entity.Register;
import com.my.Entity.ServiceHours;
import com.my.Entity.Users;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author user
 */
@Named(value = "ContextMenuView")
@RequestScoped
public class ContextMenuView implements Serializable {

    @EJB
    VolunteerBeanLocal volunteerService;
    @EJB
    TrainingEJBLocal trainingService;
    @EJB
    ServiceHoursEJBLocal serviceHoursService;
    @EJB
    RegisterEJBLocal registerService;
    @EJB
    UserEJBLocal userService;
    @EJB
    OrgEJBLocal orgService;

    private BasicInformationEntity SelectedVolunteer;
    private EducationTraining SelectedET;
    private ServiceHours SelectedSH;
    private Register SelectedR;
    private Users SelectedUser;
    private Org SelectedOrg;
    private long id;
    private String idNB;

    /**
     * Creates a new instance of search_volunteer
     */
    public ContextMenuView() {
    }

    /**
     * 依據不同登入者階層，顯示其下層的所有志工資料
     *
     * @return
     */
    public List<BasicInformationEntity> getAllVolunteers() {
        String login_account = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
        String login_orgCode = userService.findUser(login_account).getOrgCode();
        //放入要回傳的志工基本資料
        List<BasicInformationEntity> setList = new ArrayList<BasicInformationEntity>();
        //若登入者為地方主管機關
        if (login_orgCode.length() == 1) {
            return volunteerService.allVolunteers();

            //若登入者為目的事業主管機關
        } else if (login_orgCode.length() == 2) {
            //取出運用單位組織代碼陣列，這些代碼父層皆等於登入者所在的組織代碼，如此即可取出該目的主管機關下的運用單位代碼
            for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode(login_orgCode)) {
                //取出志工隊組織代碼陣列，這些代碼父層皆等於剛剛取出來的運用單位代碼，如此即可取出這些運用單位下的志工隊代碼
                for (Org id2 : (List<Org>) orgService.findSpecific_OrgByParentCode(id.getOrgCode())) {
                    //透過剛剛取出來的志工隊代碼，找出代碼相符的志工基本資料
                    setList.addAll(volunteerService.allVolunteers_ByOrgCode(id2.getOrgCode()));
                }
            }
            return setList;
            //若登入者為運用單位
        } else if (login_orgCode.length() == 3) {
            //取出志工隊組織代碼陣列，這些代碼父層皆等於登入者所在的運用單位代碼，如此即可取出該運用單位下的志工隊代碼
            for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode(login_orgCode)) {
                //透過剛剛取出來的志工隊代碼，找出代碼相符的志工基本資料
                setList.addAll(volunteerService.allVolunteers_ByOrgCode(id.getOrgCode()));
            }
            return setList;
            //若登入者為志工隊
        } else {
            //取得該志工隊所有的志工基本資料
            return volunteerService.allVolunteers_ByOrgCode(login_orgCode);
        }
    }

    /**
     * 依據不同登入者階層，顯示其下層的所有教育訓練資料
     *
     * @return
     */
    public List<EducationTraining> getAllETs() {
        String login_account = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
        String login_orgCode = userService.findUser(login_account).getOrgCode();
        //放入要回傳的志工基本資料
        List<EducationTraining> setList = new ArrayList<EducationTraining>();
        //若登入者為地方主管機關
        if (login_orgCode.length() == 1) {
            return trainingService.allEducationTraining();
            //若登入者為目的事業主管機關
        } else if (login_orgCode.length() == 2) {
            //取出運用單位組織代碼陣列，這些代碼父層皆等於登入者所在的組織代碼，如此即可取出該目的主管機關下的運用單位代碼
            for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode(login_orgCode)) {
                //取出志工隊組織代碼陣列，這些代碼父層皆等於剛剛取出來的運用單位代碼，如此即可取出這些運用單位下的志工隊代碼
                for (Org id2 : (List<Org>) orgService.findSpecific_OrgByParentCode(id.getOrgCode())) {
                    //透過剛剛取出來的志工隊代碼，找出代碼相符的志工基本資料
                    setList.addAll(trainingService.allEducationTraining_ByOrgCode(id2.getOrgCode()));
                }
            }
            return setList;
            //若登入者為運用單位
        } else if (login_orgCode.length() == 3) {
            //取出志工隊組織代碼陣列，這些代碼父層皆等於登入者所在的運用單位代碼，如此即可取出該運用單位下的志工隊代碼
            for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode(login_orgCode)) {
                //透過剛剛取出來的志工隊代碼，找出代碼相符的志工基本資料
                setList.addAll(trainingService.allEducationTraining_ByOrgCode(id.getOrgCode()));
            }
            return setList;
            //若登入者為志工隊
        } else {
            //取得該志工隊所有的志工基本資料
            return trainingService.allEducationTraining_ByOrgCode(login_orgCode);
        }
    }

    /**
     * 依據不同登入者階層，顯示其下層的所有服務時數資料
     *
     * @return
     */
    public List<ServiceHours> getAllSHs() {
        String login_account = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
        String login_orgCode = userService.findUser(login_account).getOrgCode();
        //放入要回傳的志工基本資料
        List<ServiceHours> setList = new ArrayList<ServiceHours>();
        //若登入者為地方主管機關
        if (login_orgCode.length() == 1) {
            return serviceHoursService.allServiceHours();
            //若登入者為目的事業主管機關
        } else if (login_orgCode.length() == 2) {
            //取出運用單位組織代碼陣列，這些代碼父層皆等於登入者所在的組織代碼，如此即可取出該目的主管機關下的運用單位代碼
            for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode(login_orgCode)) {
                //取出志工隊組織代碼陣列，這些代碼父層皆等於剛剛取出來的運用單位代碼，如此即可取出這些運用單位下的志工隊代碼
                for (Org id2 : (List<Org>) orgService.findSpecific_OrgByParentCode(id.getOrgCode())) {
                    //透過剛剛取出來的志工隊代碼，找出代碼相符的志工基本資料
                    setList.addAll(serviceHoursService.allServiceHours_ByOrgCode(id2.getOrgCode()));
                }
            }
            return setList;
            //若登入者為運用單位
        } else if (login_orgCode.length() == 3) {
            //取出志工隊組織代碼陣列，這些代碼父層皆等於登入者所在的運用單位代碼，如此即可取出該運用單位下的志工隊代碼
            for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode(login_orgCode)) {
                //透過剛剛取出來的志工隊代碼，找出代碼相符的志工基本資料
                setList.addAll(serviceHoursService.allServiceHours_ByOrgCode(id.getOrgCode()));
            }
            return setList;
            //若登入者為志工隊
        } else {
            //取得該志工隊所有的志工基本資料
            return serviceHoursService.allServiceHours_ByOrgCode(login_orgCode);
        }
    }

    /**
     * 依據不同登入者階層，顯示其下層的所有紀錄冊資料
     *
     * @return
     */
    public List<Register> getAllRs() {
        String login_account = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
        String login_orgCode = userService.findUser(login_account).getOrgCode();
        //放入要回傳的志工基本資料
        List<Register> setList = new ArrayList<Register>();
        //若登入者為地方主管機關
        if (login_orgCode.length() == 1) {
            return registerService.allRegister();
            //若登入者為目的事業主管機關
        } else if (login_orgCode.length() == 2) {
            //取出運用單位組織代碼陣列，這些代碼父層皆等於登入者所在的組織代碼，如此即可取出該目的主管機關下的運用單位代碼
            for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode(login_orgCode)) {
                //取出志工隊組織代碼陣列，這些代碼父層皆等於剛剛取出來的運用單位代碼，如此即可取出這些運用單位下的志工隊代碼
                for (Org id2 : (List<Org>) orgService.findSpecific_OrgByParentCode(id.getOrgCode())) {
                    //透過剛剛取出來的志工隊代碼，找出代碼相符的志工基本資料
                    setList.addAll(registerService.allRegister_ByOrgCode(id2.getOrgCode()));
                }
            }
            return setList;
            //若登入者為運用單位
        } else if (login_orgCode.length() == 3) {
            //取出志工隊組織代碼陣列，這些代碼父層皆等於登入者所在的運用單位代碼，如此即可取出該運用單位下的志工隊代碼
            for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode(login_orgCode)) {
                //透過剛剛取出來的志工隊代碼，找出代碼相符的志工基本資料
                setList.addAll(registerService.allRegister_ByOrgCode(id.getOrgCode()));
            }
            return setList;
            //若登入者為志工隊
        } else {
            //取得該志工隊所有的志工基本資料
            return registerService.allRegister_ByOrgCode(login_orgCode);
        }
    }

    /**
     * 回傳所有使用者資料
     *
     * @return
     */
    public List<Users> getAllUsers() {
        return userService.allUser();
    }

    /**
     * 回傳所有組織單位資料
     *
     * @return
     */
    public List<Org> getAllOrg() {
        return orgService.allOrgs();
    }

    /**
     * 換算志工的年齡。今年-出生國民年=年齡
     *
     * @param birthday
     * @return
     */
    public int getAge(String birthday) {
        String[] born_part = birthday.split("-");
        int born_year = Integer.parseInt(born_part[0]);
        int now = Calendar.getInstance().get(Calendar.YEAR);
        int age = now - born_year;
        return age;
    }

    /**
     * 轉換父層組織代號為名稱
     *
     * @param convertParentCode
     * @return
     */
    public String getConvertParentCode(String convertParentCode) {
        if ("0".equals(convertParentCode)) {
            return "無父層";
        }
        return orgService.findSingleOrg_ByOrgCode(convertParentCode).getOrgName();
    }

    /**
     * 轉換組織代號為名稱
     *
     * @param convertOrgCode
     * @return
     */
    public String getConvertOrgCode(String convertOrgCode) {
        return orgService.findSingleOrg_ByOrgCode(convertOrgCode).getOrgName();
    }

    /**
     * 轉換日期格式為七碼
     *
     * @param date
     * @return
     */
    public String getConvertDate_7(String date) {
        String[] date_part = date.split("-");
        int date_year = Integer.parseInt(date_part[0]) - 1911;
        String formatStr_year = String.format("%03d", date_year);
        return formatStr_year + date_part[1] + date_part[2];
    }

    /**
     * 轉換紀錄冊字代號為名稱
     *
     * @param recDomainWord
     * @return
     */
    public String getConvertRecDomainWord(String recDomainWord) {
        return registerService.findRecDomainWord(recDomainWord).getRecDomainWord();
    }

    /**
     * 轉換紀錄冊字領取紀錄代碼為名稱
     *
     * @param conferType
     * @return
     */
    public String getConvertConferType(String conferType) {
        String returnString = "";
        switch (conferType) {
            case "2":
                returnString = "初次";
                break;
            case "3":
                returnString = "換發";
                break;
            case "4":
                returnString = "補發";
                break;
            case "5":
                returnString = "註銷";
                break;
        }
        return returnString;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIdNB() {
        return idNB;
    }

    public void setIdN(String idNB) {
        this.idNB = idNB;
    }

    public BasicInformationEntity getSelectedVolunteer() {
        return SelectedVolunteer;
    }

    /**
     * 紀錄被使用者選取的志工資料列之身分證字號
     *
     * @param SelectedVolunteer
     */
    public void setSelectedVolunteer(BasicInformationEntity SelectedVolunteer) {
        this.SelectedVolunteer = SelectedVolunteer;
        if (SelectedVolunteer != null) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("idNumber", this.SelectedVolunteer.getIdNumber());
        }
    }

    public EducationTraining getSelectedET() {
        return SelectedET;
    }

    /**
     * 紀錄被使用者選取的志工教育訓練資料列之ID與身分證字號
     *
     * @param SelectedET
     */
    public void setSelectedET(EducationTraining SelectedET) {
        this.SelectedET = SelectedET;
        if (SelectedET != null) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("idNumber", this.SelectedET.getIdNumber());
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("id", this.SelectedET.getId());
        }
    }

    public ServiceHours getSelectedSH() {
        return SelectedSH;
    }

    /**
     * 紀錄被使用者選取的志工服務時數資料列之ID與身分證字號
     *
     * @param SelectedSH
     */
    public void setSelectedSH(ServiceHours SelectedSH) {
        this.SelectedSH = SelectedSH;
        if (SelectedSH != null) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("idNumber", this.SelectedSH.getIdNumber());
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("id", this.SelectedSH.getId());
        }
    }

    public Register getSelectedR() {
        return SelectedR;
    }

    /**
     * 紀錄被使用者選取的志工紀錄冊資料列之ID與身分證字號
     *
     * @param SelectedR
     */
    public void setSelectedR(Register SelectedR) {
        this.SelectedR = SelectedR;
        if (SelectedR != null) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("idNumber", this.SelectedR.getIdNumber());
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("id", this.SelectedR.getId());
        }
    }

    public Users getSelectedUser() {
        return SelectedUser;
    }

    /**
     * 紀錄被使用者選取的使用者資料列之帳號
     *
     * @param SelectedUser
     */
    public void setSelectedUser(Users SelectedUser) {
        this.SelectedUser = SelectedUser;
        if (SelectedUser != null) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("account", this.SelectedUser.getAccount());
        }
    }

    public Org getSelectedOrg() {
        return SelectedOrg;
    }

    /**
     * 紀錄被使用者選取的使用者資料列之帳號
     *
     * @param SelectedOrg
     */
    public void setSelectedOrg(Org SelectedOrg) {
        this.SelectedOrg = SelectedOrg;
        if (SelectedOrg != null) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("orgCode", this.SelectedOrg.getOrgCode());
        }
    }

    /**
     * Remove the selected item;(基本資料)
     */
    public void deleteVolunteer() {
        volunteerService.remove(this.SelectedVolunteer);
        SelectedVolunteer = null;
    }

    /**
     * Remove the selected item;(教育訓練)
     */
    public void deleteET() {
        trainingService.removeET(this.SelectedET);
        SelectedET = null;
    }

    /**
     * Remove the selected item;(服務時數)
     */
    public void deleteSH() {
        serviceHoursService.removeSH(this.SelectedSH);
        SelectedSH = null;
    }

    /**
     * Remove the selected item;(紀錄冊)
     */
    public void deleteR() {
        registerService.removeRegiser(this.SelectedR);
        SelectedR = null;
    }

    /**
     * Remove the selected item;(使用者)
     */
    public void deleteUser() {
        userService.removeUser(this.SelectedUser);
        SelectedUser = null;
    }

    /**
     * Remove the selected item;(組織單位)
     */
    public void deleteOrg() {
        List<Org> setList = new ArrayList<Org>();
        List<Org> setList2 = new ArrayList<Org>();
        //依據被刪除的組織單位刪除對應的組織單位資料
        if (4 == this.SelectedOrg.getOrgCode().length()) {
            //刪除該單位下的所有志工基本資料
            volunteerService.removeByOrgCode(this.SelectedOrg.getOrgCode());
            //刪除該單位下的所有教育訓練資料
            trainingService.removeByOrgCode(this.SelectedOrg.getOrgCode());
            //刪除該單位下的所有服務時數資料
            serviceHoursService.removeByOrgCode(this.SelectedOrg.getOrgCode());
            //刪除該單位下的所有紀錄冊資料
            registerService.removeByOrgCode(this.SelectedOrg.getOrgCode());
            //刪除志工隊
            orgService.removeOrg(this.SelectedOrg);
            SelectedOrg = null;
        } else if (3 == this.SelectedOrg.getOrgCode().length()) {//刪除單一運用單位及該運用單位下志工隊
            //放入該運用單位下志工隊
            setList.addAll(getChildOrg(this.SelectedOrg.getOrgCode()));
            //刪除該單位下的所有志工基本資料
            removeVolByOrgCode(setList);
            //刪除該單位下的所有教育訓練資料
            removeEduByOrgCode(setList2);
            //刪除該單位下的所有服務時數資料
            removeSvsByOrgCode(setList2);
            //刪除該單位下的所有紀錄冊資料
            removeRecByOrgCode(setList2);
            //刪除該運用單位下的所有志工隊
            removeVolOrg(setList);
            //刪除運用單位
            orgService.removeOrg(this.SelectedOrg);
            SelectedOrg = null;

        } else if (2 == this.SelectedOrg.getOrgCode().length()) {//刪除單一目的事業主管機關、該機關下運用單位及每個運用單位下志工隊
            //setList：放入該目的事業主管機關下運用單位
            setList.addAll(getChildOrg(this.SelectedOrg.getOrgCode()));
            for (Org id : (List<Org>) setList) {
                //setList2：放入運用單位下的志工隊
                setList2.addAll(getChildOrg(id.getOrgCode()));
                //刪除該單位下的所有志工基本資料
                removeVolByOrgCode(setList2);
                //刪除該單位下的所有教育訓練資料
                removeEduByOrgCode(setList2);
                //刪除該單位下的所有服務時數資料
                removeSvsByOrgCode(setList2);
                //刪除該單位下的所有紀錄冊資料
                removeRecByOrgCode(setList2);
                //刪除該運用單位下的所有志工隊
                removeVolOrg(setList2);
                //刪除該運用單位
                orgService.removeOrg(id);
            }
            //刪除目的事業主管機關
            orgService.removeOrg(this.SelectedOrg);
            SelectedOrg = null;
        }
    }

    /**
     * 取得子層組織單位
     *
     * @param orgCode
     * @return
     */
    private List getChildOrg(String orgCode) {
        List<Org> setList = new ArrayList<Org>();
        for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode(orgCode)) {
            setList.add(id);
        }
        return setList;
    }

    /**
     * 刪除志工隊單位
     *
     * @param setList
     */
    private void removeVolOrg(List setList) {
        for (Org id : (List<Org>) setList) {
            orgService.removeOrg(id);
        }
    }

    /**
     * 刪除組織代碼於setList清單內的志工基本資料
     *
     * @param setList
     */
    private void removeVolByOrgCode(List setList) {
        for (Org id : (List<Org>) setList) {
            volunteerService.removeByOrgCode(id.getOrgCode());
        }
    }

    /**
     * 刪除組織代碼於setList清單內的教育訓練資料
     *
     * @param setList
     */
    private void removeEduByOrgCode(List setList) {
        for (Org id : (List<Org>) setList) {
            trainingService.removeByOrgCode(id.getOrgCode());
        }
    }

    /**
     * 刪除組織代碼於setList清單內的服務時數資料
     *
     * @param setList
     */
    private void removeSvsByOrgCode(List setList) {
        for (Org id : (List<Org>) setList) {
            serviceHoursService.removeByOrgCode(id.getOrgCode());
        }
    }

    /**
     * 刪除組織代碼於setList清單內的紀錄冊資料
     *
     * @param setList
     */
    private void removeRecByOrgCode(List setList) {
        for (Org id : (List<Org>) setList) {
            registerService.removeByOrgCode(id.getOrgCode());
        }
    }
}
