/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vol.bean;

import com.my.Ejb.VolunteerBeanLocal;
import com.my.Entity.BasicInformationEntity;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 * @deprecated 
 * @author user
 */
@Named(value = "dfView")
@SessionScoped
public class dfView implements Serializable {

    @EJB
    VolunteerBeanLocal volunteerService;

    /**
     * Creates a new instance of search_volunteer
     */
    public dfView() {
    }

    public void chooseVolunteers() {
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("modal", true);
        RequestContext.getCurrentInstance().openDialog("selectVolunteers", options, null);
    }

    public void selectVolunteerFromDialog() {
        RequestContext.getCurrentInstance().closeDialog("selectVolunteers");
    }
}
