/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vol.bean;

import com.my.Ejb.VolunteerBeanLocal;
import com.my.Entity.BasicInformationEntity;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;

/**
 * @deprecated 
 * @author user
 */
@Named(value = "dfVolunteersView")
@RequestScoped
public class dfVolunteersView implements Serializable {

    @EJB
    VolunteerBeanLocal volunteerService;
    private List selectedVolunteersList;
    private String filterText_Name;
    private String filterText_idName;
    List<SelectItem> setList = new ArrayList<SelectItem>();

    /**
     * Creates a new instance of search_volunteer
     */
    public dfVolunteersView() {
    }

    public List getSelectedVolunteersList() {
        return selectedVolunteersList;
    }

    public void setSelectedVolunteersList(List selectedVolunteersList) {
        this.selectedVolunteersList = selectedVolunteersList;
    }

//    public List<BasicInformationEntity> getAllVolunteers() {
//        return volunteerService.AllVolunteers("1");
//    }

    public String getFilterText_Name() {
        return filterText_Name;
    }

    public void setFilterText_Name(String filterText_Name) {
        this.filterText_Name = filterText_Name;
    }

    public String getFilterText_idName() {
        return filterText_idName;
    }

    public void setFilterText_idName(String filterText_idName) {
        this.filterText_idName = filterText_idName;
    }

//    public void filter() {
//        if (filterText_Name != null || filterText_Name.length() != 0 || filterText_idName != null || filterText_idName.length() != 0) {
//            setList.clear();
//            for (BasicInformationEntity id : volunteerService.AllVolunteers("1")) {
//                if (id.getName().contains(filterText_Name) && id.getIdNumber().contains(filterText_idName)) {
//                    setList.add(new SelectItem(id.getName() + "(" + id.getIdNumber() + ")", id.getName() + "(" + id.getIdNumber() + ")"));
//                }
//            }
//        }
//    }

    public List<SelectItem> getAllVolunteerItems() {
        return setList;
    }

    List<BasicInformationEntity> Volunteers;

    @PostConstruct
    public void init() {
        
    }

}
