/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vol.bean.edu_train;

import com.my.Ejb.OrgEJBLocal;
import com.my.Ejb.TrainingEJBLocal;
import com.my.Ejb.UserEJBLocal;
import com.my.Ejb.VolunteerBeanLocal;
import com.my.Entity.BasicInformationEntity;
import com.my.Entity.EducationTraining;
import static com.vol.util.RecordHelper.createTime;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorListener;
import javax.faces.event.ValueChangeEvent;
import org.primefaces.context.RequestContext;

/**
 *
 * @author user
 */
@Named(value = "create_EducationTraining")
@SessionScoped
public class create_EducationTraining implements Serializable {

    @EJB
    VolunteerBeanLocal volunteerService;
    @EJB
    TrainingEJBLocal trainingService;
    @EJB
    UserEJBLocal userService;
    @EJB
    OrgEJBLocal orgService;

    /**
     * 紀錄被選取要加入教育訓練的志工
     */
    private List<BasicInformationEntity> selectedItems;

    private String idNumber;
    private String Name;
    private String train_date_s;
    private String train_date_e;
    private String train_org;
    private String train_course;
    private String domain_type;
    private String times;
    private String hours;
    private String mins;
    private String course_name;
    /**
     * 備註
     */
    private String memo;
    /**
     * 核定日期
     */
    private Date[] approve_date;
    /**
     * 批次輸入所依據的核定日期欄位
     */
    private Date main_approve_date;
    /**
     * 核定名稱
     */
    private String[] approve_name;
    /**
     * 批次輸入所依據的核定名稱
     */
    private String main_approve_name;
    /**
     * 判斷是批次或分別輸入。batch(批次)或part(分別)
     */
    private String entry;
    private String org_code;

    //預設值
    @PostConstruct
    void init() {
        entry = "batch";//設為批次輸入
        times = "1";
        hours = "1";
        mins = "0";
    }

    /**
     * 設定畫面資料為預設
     */
    public void reset() {
        train_date_s = null;
        train_date_e = null;
        train_org = null;
        train_course = null;
        domain_type = null;
        times = "1";
        hours = "1";
        mins = "0";
        course_name = null;
        memo = null;
        approve_date = null;
        main_approve_date = null;
        approve_name = null;
        main_approve_name = null;
        selectedItems = null;
        entry = "batch";
    }

    /**
     * 取得使用者選取的志工隊下之志工名單
     *
     * @return
     */
    public List<BasicInformationEntity> getAllVolunteerItems() {
//        String loginAccount = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
//        if (loginAccount.length() == 4) {
//            //取得志工隊組織代碼對應的組織名稱
//            org_code = loginAccount;
//        }
        return volunteerService.allVolunteers_ByOrgCode(org_code);
    }

    public List<BasicInformationEntity> getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(List<BasicInformationEntity> selectedItems) {
        this.selectedItems = selectedItems;
    }

    public String getOrg_code() {
        return org_code;
    }

    public void setOrg_code(String org_code) {
        this.org_code = org_code;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getTrain_date_s() {
        return train_date_s;
    }

    public void setTrain_date_s(String train_date_s) {
        this.train_date_s = train_date_s;
    }

    public String getTrain_date_e() {
        return train_date_e;
    }

    public void setTrain_date_e(String train_date_e) {
        this.train_date_e = train_date_e;
    }

    public String getTrain_org() {
        return train_org;
    }

    public void setTrain_org(String train_org) {
        this.train_org = train_org;
    }

    public String getTrain_course() {
        return train_course;
    }

    public void setTrain_course(String train_course) {
        this.train_course = train_course;
    }

    public String getDomain_type() {
        return domain_type;
    }

    public void setDomain_type(String domain_type) {
        this.domain_type = domain_type;
    }

    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getMins() {
        return mins;
    }

    public void setMins(String mins) {
        this.mins = mins;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Date[] getApprove_date() {
        return approve_date;
    }

    public void setApprove_date(Date[] approve_date) {
        this.approve_date = approve_date;
    }

    public Date getMain_approve_date() {
        return main_approve_date;
    }

    public void setMain_approve_date(Date main_approve_date) {
        this.main_approve_date = main_approve_date;
    }

    public String[] getApprove_name() {
        return approve_name;
    }

    public void setApprove_name(String[] approve_name) {
        this.approve_name = approve_name;
    }

    public String getMain_approve_name() {
        return main_approve_name;
    }

    public void setMain_approve_name(String main_approve_name) {
        this.main_approve_name = main_approve_name;
    }

    public String getEntry() {
        return entry;
    }

    public void setEntry(String entry) {
        this.entry = entry;
    }

    public create_EducationTraining() {
    }

    /**
     * 批次與分別輸入的Ajax Listener設定
     *
     * @param event
     */
    public void batchAjax(AjaxBehaviorListener event) {
        //若使用者點選批次輸入，將所有所有選取的志工資料(核定日期、核定名稱)更新為與上方批次輸入的欄位相同。
        if (entry.equals("batch")) {
            try {
                for (int i = 0; i < approve_date.length; i++) {
                    Date s = getMain_approve_date();
                    String n = getMain_approve_name();
                    approve_date[i] = getMain_approve_date();
                    approve_name[i] = getMain_approve_name();
                }
            } catch (NullPointerException e) {
            }
        }
    }

    /**
     * 執行儲存。 判斷有無必填欄位未填，或是輸入資料有誤，將回傳錯誤訊息。 若沒問題，將資料儲存至資料庫。
     *
     * @return
     */
    public String save() {
        //找出必填  卻未填的欄位 (因為在頁面上設置required="true"，Dialog跳不出來)
        String[] required = new String[8];
        required[0] = train_date_s;
        required[1] = train_date_e;
        required[2] = train_org;
        required[3] = train_course;
        required[4] = domain_type;
        required[5] = times;
        required[6] = hours;
        required[7] = mins;
        boolean nu = false;
        boolean er = false;
        //判斷有無填寫教育訓練資料
        int year_s = 0;
        int year_e = 999999999;
        for (int i = 0; i < required.length; i++) {
            if (required[i] == null || required[i].length() == 0) {
                nu = true;
            } else {
                if (i == 0 || i == 1) {
                    String[] y_part = required[i].split("-");
                    int year = Integer.parseInt(y_part[0] + y_part[1] + y_part[2]);
                    if (i == 0) {
                        year_s = year;
                    }
                    if (i == 1) {
                        year_e = year;
                    }
                    if (year < 20010120) {
                        er = true;
                        FacesContext.getCurrentInstance().addMessage("form:msg", new FacesMessage("訓綀日期起迄不可早於2001/1/20"));
                    }
                    if (year_e < year_s) {
                        er = true;
                        FacesContext.getCurrentInstance().addMessage("form:msg", new FacesMessage("訓綀日期迄不可早於起"));
                    }
                    if (y_part[0].length() > 4) {
                        er = true;
                        FacesContext.getCurrentInstance().addMessage("form:msg", new FacesMessage("年份格式有誤，請再確認"));
                    }
                } else if (i == 5) {
                    if (Integer.parseInt(required[i]) < 1 || Integer.parseInt(required[i]) > 99) {
                        er = true;
                        FacesContext.getCurrentInstance().addMessage("form:msg", new FacesMessage("訓練次數範圍必須介於1~99"));
                    }
                } else if (i == 6) {
                    if (Integer.parseInt(required[i]) < 1 || Integer.parseInt(required[i]) > 999999999) {
                        er = true;
                        FacesContext.getCurrentInstance().addMessage("form:msg", new FacesMessage("訓練時數-小時範圍必須介於1~999999999，不可為0"));
                    }
                } else if (i == 7) {
                    if (Integer.parseInt(required[i]) < 0 || Integer.parseInt(required[i]) > 59) {
                        er = true;
                        FacesContext.getCurrentInstance().addMessage("form:msg", new FacesMessage("訓練時數-分鐘範圍必須介於0~59"));
                    }
                }
            }
        }
        //判斷有無選擇志工、填寫欄位
        try {
            for (int i = 0; i < approve_date.length; i++) {
                if (approve_date[i] == null || approve_name[i] == null || approve_name[i].length() == 0) {
                    nu = true;
                }
            }
        } catch (NullPointerException e) {
            FacesContext.getCurrentInstance().addMessage("form:msg", new FacesMessage("請選擇至少一位志工"));
            er = true;
        }

        //顯示提示訊息
        if (nu || er) {
            if (nu) {
                FacesContext.getCurrentInstance().addMessage("form:msg", new FacesMessage("請輸入所有必填欄位！"));
            }
            return "create_EducationTraining.xhtml";
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
            for (int i = 0; i < approve_date.length; i++) {
                idNumber = selectedItems.get(i).getIdNumber();
                Name = selectedItems.get(i).getName();
                //存入資料庫
                EducationTraining et = new EducationTraining();
                et.setIdNumber(idNumber);
                et.setName(Name);
                et.setTrain_date_s(train_date_s);
                et.setTrain_date_e(train_date_e);
                et.setTrain_org(train_org);
                et.setTrain_course(train_course);
                et.setDomain_type(domain_type);
                et.setTimes(times);
                et.setHours(hours);
                et.setMins(mins);
                et.setCourse_name(course_name);
                et.setMemo(memo);
                et.setApprove_date(sdf.format(approve_date[i]));
                et.setApprove_name(approve_name[i]);
                et.setOrg_code(org_code);
                et.setCreate_time(createTime());
                trainingService.addEducationTraining(et);
            }
            return "search_EducationTraining";
        }
    }

    /**
     * 開啟selectVolunteers_ET對話框，供使用者選取志工
     */
    public void chooseVolunteers() {
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("draggable", true);
        RequestContext.getCurrentInstance().openDialog("selectVolunteers_ET", options, null);
    }

    /**
     * 設定核定日期及核定名稱的陣列數量
     */
    public void onVolunteersChosen() {
        approve_date = new Date[selectedItems.size()];
        approve_name = new String[selectedItems.size()];
    }

    /**
     * 關閉selectVolunteers_ET對話框
     */
    public void closeDialog() {
        RequestContext.getCurrentInstance().closeDialog("selectVolunteers_ET");
    }
}
