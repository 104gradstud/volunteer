/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vol.bean.edu_train;

import com.my.Ejb.TrainingEJBLocal;
import com.my.Ejb.VolunteerBeanLocal;
import com.my.Entity.EducationTraining;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.sql.Date;

import javax.ejb.EJB;
import javax.faces.context.FacesContext;

/**
 *
 * @author user
 */
@Named(value = "update_ET")
@SessionScoped
public class update_EducationTraining implements Serializable {

//    @EJB
//    VolunteerBeanLocal volunteerService;
    @EJB
    TrainingEJBLocal trainingService;
    EducationTraining et;

    private Long id;
    private String idNumber;
    private String Name;
    private String train_date_s;
    private String train_date_e;
    private String train_org;
    private String train_course;
    private String domain_type;
    private String times;
    private String hours;
    private String mins;
    private String course_name;
    private String memo;
    /**
     * 核准日期
     */
    private String approve_date;
    /**
     * 核准名稱
     */
    private String approve_name;

    /**
     * 設定畫面資料為預設
     */
    public void reset() {
        train_date_s = null;
        train_date_e = null;
        train_org = null;
        train_course = null;
        domain_type = null;
        times = "1";
        hours = "1";
        mins = "0";
        course_name = null;
        memo = null;
        approve_date = null;
        approve_name = null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getTrain_date_s() {
        return train_date_s;
    }

    public void setTrain_date_s(String train_date_s) {
        this.train_date_s = train_date_s;
    }

    public String getTrain_date_e() {
        return train_date_e;
    }

    public void setTrain_date_e(String train_date_e) {
        this.train_date_e = train_date_e;
    }

    public String getTrain_org() {
        return train_org;
    }

    public void setTrain_org(String train_org) {
        this.train_org = train_org;
    }

    public String getTrain_course() {
        return train_course;
    }

    public void setTrain_course(String train_course) {
        this.train_course = train_course;
    }

    public String getDomain_type() {
        return domain_type;
    }

    public void setDomain_type(String domain_type) {
        this.domain_type = domain_type;
    }

    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getMins() {
        return mins;
    }

    public void setMins(String mins) {
        this.mins = mins;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getApprove_date() {
        return approve_date;
    }

    public void setApprove_date(String approve_date) {
        this.approve_date = approve_date;
    }

    public String getApprove_name() {
        return approve_name;
    }

    public void setApprove_name(String approve_name) {
        this.approve_name = approve_name;
    }

    /**
     * Creates a new instance of create
     */
    public update_EducationTraining() {
    }

    /**
     * 設定使用者要查看的該筆教育訓練資料
     * @return 
     */
    public String ViewET() {
        idNumber = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("idNumber");
        id = (Long) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("id");
        et = (EducationTraining) trainingService.findEducationTraining(idNumber, id);
        setName(et.getName());
        setIdNumber(et.getIdNumber());
        setTrain_date_s(et.getTrain_date_s());
        setTrain_date_e(et.getTrain_date_e());
        setTrain_org(et.getTrain_org());
        setTrain_course(et.getTrain_course());
        setDomain_type(et.getDomain_type());
        setTimes(et.getTimes());
        setHours(et.getHours());
        setMins(et.getMins());
        setApprove_date(et.getApprove_date());
        setApprove_name(et.getApprove_name());
        setCourse_name(et.getCourse_name());
        setMemo(et.getMemo());

        return "update_EducationTraining.xhtml";
    }

    /**
     * 更新教育訓練資料
     * @return 
     */
    public String update() {
        //設置參數
        EducationTraining et = new EducationTraining();
        et.setId(id);
        et.setIdNumber(idNumber);
        et.setName(Name);
        et.setTrain_date_s(train_date_s);
        et.setTrain_date_e(train_date_e);
        et.setTrain_org(train_org);
        et.setTrain_course(train_course);
        et.setDomain_type(domain_type);
        et.setTimes(times);
        et.setHours(hours);
        et.setMins(mins);
        et.setCourse_name(course_name);
        et.setMemo(memo);
        et.setApprove_date(approve_date);
        et.setApprove_name(approve_name);
        //存入資料庫
        trainingService.updateET(et);
        return "search_EducationTraining";
    }

}
