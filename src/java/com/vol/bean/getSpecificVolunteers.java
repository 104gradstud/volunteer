/*
 * 負責各個新增頁面取得特定志工隊。
 例如：於新增教育訓練資料頁面選擇目的事業主管機關後，接著選擇該目的事業主管機關下的運用單位，最後選擇該運用單位下的志工隊。
 */
package com.vol.bean;

import com.my.Ejb.OrgEJBLocal;
import com.my.Ejb.RegisterEJBLocal;
import com.my.Ejb.ServiceHoursEJBLocal;
import com.my.Ejb.TrainingEJBLocal;
import com.my.Ejb.UserEJBLocal;
import com.my.Ejb.VolunteerBeanLocal;
import com.my.Entity.Org;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author user
 */
@Named(value = "getSpecificVolunteers")
@SessionScoped
public class getSpecificVolunteers implements Serializable {

    @EJB
    VolunteerBeanLocal volunteerService;
    @EJB
    TrainingEJBLocal trainingService;
    @EJB
    ServiceHoursEJBLocal serviceHoursService;
    @EJB
    RegisterEJBLocal registerService;
    @EJB
    UserEJBLocal userService;
    @EJB
    OrgEJBLocal orgService;

    /**
     * 目的事業主管機關欄位編號
     */
    private String selectedOrg_Code2Layer;
    /**
     * 運用單位欄位編號
     */
    private String selectedOrg_Code3Layer;
    /**
     * 志工隊欄位編號
     */
    private String selectedOrg_Code4Layer;

    public getSpecificVolunteers() {
    }

    public String getSelectedOrg_Code2Layer() {
        return selectedOrg_Code2Layer;
    }

    public void setSelectedOrg_Code2Layer(String selectedOrg_Code2Layer) {
        this.selectedOrg_Code2Layer = selectedOrg_Code2Layer;
    }

    public String getSelectedOrg_Code3Layer() {
        return selectedOrg_Code3Layer;
    }

    public void setSelectedOrg_Code3Layer(String selectedOrg_Code3Layer) {
        this.selectedOrg_Code3Layer = selectedOrg_Code3Layer;
    }

    public String getSelectedOrg_Code4Layer() {
        return selectedOrg_Code4Layer;
    }

    public void setSelectedOrg_Code4Layer(String selectedOrg_Code4Layer) {
        this.selectedOrg_Code4Layer = selectedOrg_Code4Layer;
    }

    //用來記錄登入者帳號
    private String loginAccount = "";

    public String getLoginAccount() {
        return loginAccount;
    }

    public void setLoginAccount(String loginAccount) {
        this.loginAccount = loginAccount;
    }

    /**
     * 以下三個方法：getSpecific_Org2Layer、getSpecific_Org3Layer、getSpecific_Org4Layer、getSingle_Org4Layer
     * 目的是讓使用者選擇志工隊。 例如：當地方主管機關階層登入，要選擇志工隊，就要先選目的事業主管機關、運用單位、志工隊。
     * getSpecific_Org2Layer：取得特定的目的事業主管機關單位清單，並回傳
     *
     * @return
     */
    public List<SelectItem> getSpecific_Org2Layer() {
        List<SelectItem> setList = new ArrayList<SelectItem>();
        setList.add(new SelectItem("", "[目的事業主管機關]"));
        //取得目前登入者的帳號
        loginAccount = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
        //用帳號去搜尋目前登入者所在的組織單位之單位代碼org_Code，利用此代碼去找到下階層單位
        String org_Code2Layer = userService.findUser(loginAccount).getOrgCode();
        for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode(org_Code2Layer)) {
            setList.add(new SelectItem(id.getOrgCode(), id.getOrgName()));
        }
        return setList;
    }

    /**
     * 取得特定的運用單位清單，並回傳
     *
     * @return
     */
    public List<SelectItem> getSpecific_Org3Layer() {
        List<SelectItem> setList = new ArrayList<SelectItem>();
        setList.add(new SelectItem("", "[運用單位]"));
        String org_Code2Layer = "";
        if ("" == loginAccount) {
            //取得目前登入者的帳號
            loginAccount = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
            //用帳號去搜尋目前登入者所在的組織單位之單位代碼org_Code，待會利用此代碼去找到下階層單位
            org_Code2Layer = userService.findUser(loginAccount).getOrgCode();
            selectedOrg_Code2Layer = org_Code2Layer;
        }
        for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode(selectedOrg_Code2Layer)) {
            setList.add(new SelectItem(id.getOrgCode(), id.getOrgName()));
        }
        return setList;
    }

    /**
     * 取得特定的志工隊，並回傳
     *
     * @return
     */
    public List<SelectItem> getSpecific_Org4Layer() {
        List<SelectItem> setList = new ArrayList<SelectItem>();
        setList.add(new SelectItem("", "[志工隊]"));
        if ("" == loginAccount) {
            //取得目前登入者的帳號
            loginAccount = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
            //用帳號去搜尋目前登入者所在的組織單位之單位代碼org_Code，待會利用此代碼去找到下階層單位
            selectedOrg_Code3Layer = userService.findUser(loginAccount).getOrgCode();
            //加入該運用單位底下的志工隊到陣列中
            for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode(selectedOrg_Code3Layer)) {
                setList.add(new SelectItem(id.getOrgCode(), id.getOrgName()));
            }
        } else {
            //若登入者為運用單位以上階層，取得使用者選擇的運用單位代碼，利用此單位代碼取得下層志工隊
            for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode(selectedOrg_Code3Layer)) {
                setList.add(new SelectItem(id.getOrgCode(), id.getOrgName()));
            }
        }
        return setList;
    }

    /**
     * 若登入者為志工隊，取得該單位選項回傳。
     *
     * @return
     */
    public List<SelectItem> getSingle_Org4Layer() {
        List<SelectItem> setList = new ArrayList<SelectItem>();
        loginAccount = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
        selectedOrg_Code4Layer = userService.findUser(loginAccount).getOrgCode();
        Org o = orgService.findSingleOrg_ByOrgCode(selectedOrg_Code4Layer);
        setList.add(new SelectItem("", "[志工隊]"));
        setList.add(new SelectItem(o.getOrgCode(), o.getOrgName()));
        return setList;
    }
}
