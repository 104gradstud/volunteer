/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vol.bean;

import com.my.Ejb.OrgEJBLocal;
import com.my.Ejb.UserEJBLocal;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author user
 */
@Named
@ApplicationScoped
public class login implements java.io.Serializable {

    @EJB
    UserEJBLocal userService;
    @EJB
    OrgEJBLocal orgService;

    private String username;
    private String password;
    //記錄錯誤訊息
    private String msg;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMsg() {
        return msg;
    }

    public void getMsg(String msg) {
        this.msg = msg;
    }

    /**
     * Creates a new instance of login
     */
    public login() {

    }

    public boolean isUserAllowedAccess() {

//        Principal user = externalContext.getUserPrincipal();
//        Principal user = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal();
        String usernane = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteUser();
        boolean result = (usernane != null);
        return result;
//        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole("admin");
    }

    public boolean isVolunteerManagerAllowedAccess() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        boolean result = (ec.isUserInRole("admin") || ec.isUserInRole("VolunteerManager"));
        return result;
//        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole("VolunteerManager");
    }

    public boolean isEduTrainingManagerAllowedAccess() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        boolean result = (ec.isUserInRole("admin") || ec.isUserInRole("EduTrainingManager"));
        return result;
//        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole("EduTrainingManager");
    }

    public boolean isHourOfServiceManagerAllowedAccess() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        boolean result = (ec.isUserInRole("admin") || ec.isUserInRole("HourOfServiceManager"));
        return result;
//        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole("HourOfServiceManager");
    }

    public boolean isRegisterManagerAllowedAccess() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        boolean result = (ec.isUserInRole("admin") || ec.isUserInRole("RegisterManager"));
        return result;
//        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole("RegisterManager");
    }

    public boolean isAdminAccess() {
        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole("admin");
    }

    /**
     * 判斷登入者是否屬於第一階層的地方主管機關
     *
     * @return
     */
    public int isLayerAllowedAccess() {
        String loginAccount = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
        //用帳號去搜尋目前登入者所在的組織單位之單位代碼org_Code
        String org_CodeLayer = userService.findUser(loginAccount).getOrgCode();
        return org_CodeLayer.length();
    }

    /**
     * 判斷登入者是否屬於第二階層的目的事業主管機關
     *
     * @return
     */
//    public boolean isLayer2AllowedAccess() {
//        String loginAccount = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
//        //用帳號去搜尋目前登入者所在的組織單位之單位代碼org_Code
//        String org_Code2Layer = userService.findUser(loginAccount).getOrgCode();
//        return org_Code2Layer.length() == 2;
//    }
//    
//    /**
//     * 判斷登入者是否屬於第三階層的運用單位
//     * @return 
//     */
//    public boolean isLayer3AllowedAccess() {
//        String loginAccount = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
//        //用帳號去搜尋目前登入者所在的組織單位之單位代碼org_Code
//        String org_Code2Layer = userService.findUser(loginAccount).getOrgCode();
//        return org_Code2Layer.length() == 3;
//    }
//    
//    /**
//     * 判斷登入者是否屬於第四階層的志工隊
//     * @return 
//     */
//    public boolean isLayer4AllowedAccess() {
//        String loginAccount = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
//        //用帳號去搜尋目前登入者所在的組織單位之單位代碼org_Code
//        String org_Code2Layer = userService.findUser(loginAccount).getOrgCode();
//        return org_Code2Layer.length() == 4;
//    }
    public String send() {
        if (this.username.equals("") || this.password.equals("")) {
            // Bring the error message using the Faces Context
            String errorMessage = FacesContext.getCurrentInstance().getApplication().
                    getResourceBundle(FacesContext.getCurrentInstance(), "msg").
                    getString("login_err");
            // Add View Faces Message
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    errorMessage, errorMessage);
            // Add the message into context for a specific component
            FacesContext.getCurrentInstance().addMessage("login", message);
            return "error";
        } else {
            return "index";
        }

    }

    /**
     * 取得當前使用者名
     *
     * @return
     */
    public String getCurrentUserName() {
        String name = "";
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();

        if (externalContext.getUserPrincipal() != null) {
            name = externalContext.getUserPrincipal().getName(); // null
        }
        return name;
    }

}
