/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vol.bean;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@Named(value = "logout")
@SessionScoped

public class logout implements java.io.Serializable {

    /**
     * Creates a new instance of logout
     */
    public String logout() throws ServletException {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        ((HttpServletRequest) ec.getRequest()).logout();
                FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "login";
    }

}
