/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vol.bean.org;

import com.my.Ejb.OrgEJBLocal;
import com.my.Ejb.UserEJBLocal;
import com.my.Entity.Org;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.model.SelectItem;
import javax.persistence.PostLoad;
import org.primefaces.context.RequestContext;

/**
 *
 * @author user
 */
@Named(value = "create_Org")
@SessionScoped
public class create_Org implements Serializable {

    @EJB
    UserEJBLocal userService;
    @EJB
    OrgEJBLocal orgService;

    //紀錄聯絡人姓名
    private String contactName;
    //紀錄聯絡人電話
    private String contactPhone;
    //紀錄組織名稱
    private String orgName;
    //紀錄父階層代碼
    private String parentCode;
    //紀錄組織要加入階層之父層。例如：要新增志工隊，父層為運用單位代碼：3
    private String layer = "1";
    /**
     * 地方主管機關欄位編號
     */
    private String selectedOrg_Code1Layer;
    /**
     * 目的事業主管機關欄位編號
     */
    private String selectedOrg_Code2Layer;
    /**
     * 運用單位欄位編號
     */
    private String selectedOrg_Code3Layer;
    /**
     * 志工隊欄位編號
     */
    private String selectedOrg_Code4Layer;

    /**
     * 父層單位代號(使用者自行輸入單位名稱的欄位)
     */
    private String parentCodeType;

    /**
     * Switch值，用於判斷是否使用顯示選擇階層列
     */
    private boolean switchValue;

    @PostConstruct
    public void init() {
        switchValue = false;
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("hide_tr()");
    }

    //設定畫面資料為預設
    public void reset() {
        orgName = "";
        parentCode = "";
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getParentCodeType() {
        return parentCodeType;
    }

    public void setParentCodeType(String parentCodeType) {
        this.parentCodeType = parentCodeType;
    }

    public boolean isSwitchValue() {
        return switchValue;
    }

    public void setSwitchValue(boolean switchValue) {
        this.switchValue = switchValue;
    }

    /**
     * 用於判斷是否顯示階層選擇的資料列(tr)
     */
    public void switchMethod() {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        if (switchValue) {
            requestContext.execute("show_tr()");
        } else {
            requestContext.execute("hide_tr()");
        }
    }

    //回傳階層代碼
    public int layerCode() {
        return Integer.parseInt(layer);
    }

    public String getLayer() {
        return layer;
    }

    public void setLayer(String layer) {
        this.layer = layer;
    }

    public String getSelectedOrg_Code1Layer() {
        return selectedOrg_Code1Layer;
    }

    public void setSelectedOrg_Code1Layer(String selectedOrg_Code1Layer) {
        this.selectedOrg_Code1Layer = selectedOrg_Code1Layer;
    }

    public String getSelectedOrg_Code2Layer() {
        return selectedOrg_Code2Layer;
    }

    public void setSelectedOrg_Code2Layer(String selectedOrg_Code2Layer) {
        this.selectedOrg_Code2Layer = selectedOrg_Code2Layer;
    }

    public String getSelectedOrg_Code3Layer() {
        return selectedOrg_Code3Layer;
    }

    public void setSelectedOrg_Code3Layer(String selectedOrg_Code3Layer) {
        this.selectedOrg_Code3Layer = selectedOrg_Code3Layer;
    }

    public String getSelectedOrg_Code4Layer() {
        return selectedOrg_Code4Layer;
    }

    public void setSelectedOrg_Code4Layer(String selectedOrg_Code4Layer) {
        this.selectedOrg_Code4Layer = selectedOrg_Code4Layer;
    }

    public create_Org() {
    }

    /**
     * 取得使用者所選階層的組織單位清單，並回傳
     *
     * @return
     */
    public List<SelectItem> getSpecificOrgList() {
        List<SelectItem> setList = new ArrayList<SelectItem>();
        for (Org id : orgService.allOrgs()) {
            if (layerCode() == id.getOrgCode().length()) {
                setList.add(new SelectItem(id.getOrgCode(), id.getOrgName()));
            }
        }
        return setList;
    }

    //取得地方主管機關單位清單，並回傳
    public List<SelectItem> getSpecific_Org1Layer() {
        List<SelectItem> setList = new ArrayList<SelectItem>();
        setList.add(new SelectItem("", "[地方主管機關]"));
        for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode("0")) {
            setList.add(new SelectItem(id.getOrgCode(), id.getOrgName()));
        }
        return setList;
    }

    //取得特定的目的事業主管機關單位清單，並回傳
    public List<SelectItem> getSpecific_Org2Layer() {
        List<SelectItem> setList = new ArrayList<SelectItem>();
        setList.add(new SelectItem("", "[目的事業主管機關]"));
        for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode(selectedOrg_Code1Layer)) {
            setList.add(new SelectItem(id.getOrgCode(), id.getOrgName()));
        }
        return setList;
    }

    //取得特定的運用單位清單，並回傳
    public List<SelectItem> getSpecific_Org3Layer() {
        List<SelectItem> setList = new ArrayList<SelectItem>();
        setList.add(new SelectItem("", "[運用單位]"));
        for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode(selectedOrg_Code2Layer)) {
            setList.add(new SelectItem(id.getOrgCode(), id.getOrgName()));
        }
        return setList;
    }

    //取得特定的志工隊，並回傳
    public List<SelectItem> getSpecific_Org4Layer() {
        List<SelectItem> setList = new ArrayList<SelectItem>();
        setList.add(new SelectItem("", "[志工隊]"));
        for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode(selectedOrg_Code3Layer)) {
            setList.add(new SelectItem(id.getOrgCode(), id.getOrgName()));
        }
        return setList;
    }

    //儲存此筆使用者資料
    public String save() {
        //判斷使用者加入的組織階層父層，記錄父層代碼
        switch (layerCode()) {
            case 1:
                parentCode = selectedOrg_Code1Layer;
                break;
            case 2:
                parentCode = selectedOrg_Code2Layer;
                break;
            case 3:
                parentCode = selectedOrg_Code3Layer;
                break;
        }
        //設定參數
        Org o = new Org();
        o.setContactName(contactName);
        o.setContactPhone(contactPhone);
        if (switchValue) {
            o.setParentCode(parentCode);
        } else {
            o.setParentCode(parentCodeType);
        }
        o.setOrgName(orgName);
        o.setStatus("啟用");

        //取得目前欲新增的階層orgCode最大值，加上1後就是新組織的orgCode。
        List<String> setList = new ArrayList<String>();
        for (Org id : (List<Org>) orgService.allOrgs()) {
            if (layerCode() + 1 == id.getOrgCode().length()) {
                setList.add(id.getOrgCode());
            }
        }
        int maxOrgCode = Integer.parseInt(Collections.max(setList));
        String newOrgCode = String.format("%0" + String.valueOf(layerCode() + 1) + "d", maxOrgCode + 1);
        o.setOrgCode(newOrgCode);

        //存入資料庫
        orgService.addOrg(o);
        return "search_Org";
    }


}
