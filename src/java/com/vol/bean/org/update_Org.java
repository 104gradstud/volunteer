/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vol.bean.org;

import com.my.Ejb.OrgEJBLocal;
import com.my.Ejb.UserEJBLocal;
import com.my.Entity.Org;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author user
 */
@Named(value = "update_Org")
@SessionScoped
public class update_Org implements Serializable {

    @EJB
    UserEJBLocal userService;
    @EJB
    OrgEJBLocal orgService;

    //組織單位代碼
    private String orgCode;
    //紀錄聯絡人姓名
    private String contactName;
    //紀錄聯絡人電話
    private String contactPhone;
    //紀錄組織名稱
    private String orgName;
    //紀錄父階層代碼
    private String parentCode;
    //紀錄父階層單位名稱
    private String parentName;
    //紀錄父階層單位聯絡人姓名
    private String parentContactName;
    //紀錄父階層單位聯絡人電話
    private String parentContactPhone;
    //紀錄組織要加入階層之父層。例如：要新增志工隊，父層為運用單位代碼：3
    private String layer = "1";
    //紀錄組織單位狀態
    private boolean status;
    /**
     * 地方主管機關欄位編號
     */
    private String selectedOrg_Code1Layer;
    /**
     * 目的事業主管機關欄位編號
     */
    private String selectedOrg_Code2Layer;
    /**
     * 運用單位欄位編號
     */
    private String selectedOrg_Code3Layer;
    /**
     * 志工隊欄位編號
     */
    private String selectedOrg_Code4Layer;

    @PostConstruct
    public void init() {

    }

    //設定畫面資料為預設
    public void reset() {
        orgName = "";
        contactName = "";
        contactPhone = "";
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentContactName() {
        return parentContactName;
    }

    public void setParentContactName(String parentContactName) {
        this.parentContactName = parentContactName;
    }

    public String getParentContactPhone() {
        return parentContactPhone;
    }

    public void setParentContactPhone(String parentContactPhone) {
        this.parentContactPhone = parentContactPhone;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * 回傳階層代碼
     *
     * @return
     */
    public int layerCode() {
        return Integer.parseInt(layer);
    }

    public String getLayer() {
        return layer;
    }

    public void setLayer(String layer) {
        this.layer = layer;
    }

    public String getSelectedOrg_Code1Layer() {
        return selectedOrg_Code1Layer;
    }

    public void setSelectedOrg_Code1Layer(String selectedOrg_Code1Layer) {
        this.selectedOrg_Code1Layer = selectedOrg_Code1Layer;
    }

    public String getSelectedOrg_Code2Layer() {
        return selectedOrg_Code2Layer;
    }

    public void setSelectedOrg_Code2Layer(String selectedOrg_Code2Layer) {
        this.selectedOrg_Code2Layer = selectedOrg_Code2Layer;
    }

    public String getSelectedOrg_Code3Layer() {
        return selectedOrg_Code3Layer;
    }

    public void setSelectedOrg_Code3Layer(String selectedOrg_Code3Layer) {
        this.selectedOrg_Code3Layer = selectedOrg_Code3Layer;
    }

    public String getSelectedOrg_Code4Layer() {
        return selectedOrg_Code4Layer;
    }

    public void setSelectedOrg_Code4Layer(String selectedOrg_Code4Layer) {
        this.selectedOrg_Code4Layer = selectedOrg_Code4Layer;
    }

    public update_Org() {
    }

    /**
     * 設定使用者要查看的該筆組織單位資料
     *
     * @return
     */
    public String ViewOrg() {
        orgCode = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("orgCode");
        Org o = orgService.findSingleOrg_ByOrgCode(orgCode);
        setContactName(o.getContactName());
        setContactPhone(o.getContactPhone());
        setOrgName(o.getOrgName());
        if ("啟用".equals(o.getStatus())) {
            setStatus(true);
        } else {
            setStatus(false);
        }
        //若有父層，取得父層資訊
        if (!"0".equals(o.getParentCode())) {
            Org parentOrg = orgService.findSingleOrg_ByOrgCode(o.getParentCode());
            setParentName(parentOrg.getOrgName());
            setParentContactName(parentOrg.getContactName());
            setParentContactPhone(parentOrg.getContactPhone());
        }
        return "update_org.xhtml";
    }

    /**
     * 更新此筆使用者資料
     *
     * @return
     */
    public String update() {
        //設定參數
        Org o = new Org();
        if (status) {
            o.setStatus("啟用");
        } else {
            //若該單位有下層，不允許作廢
            if (!orgService.findSpecific_OrgByParentCode(orgCode).isEmpty()) {
                String msg = "該單位有下層，不允許作廢。";
                FacesContext.getCurrentInstance().addMessage("form:msg", new FacesMessage(msg));
                return "update_org.xhtml";
            } else {
                o.setStatus("作廢");
            }
        }
        o.setOrgCode(orgCode);
        o.setContactName(contactName);
        o.setContactPhone(contactPhone);
        o.setOrgName(orgName);
        
        //存入資料庫
        orgService.updateOrg(o);
        return "search_Org";
    }

}
