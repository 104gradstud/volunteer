/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vol.bean.register_book;

import com.my.Ejb.OrgEJBLocal;
import com.my.Ejb.RegisterEJBLocal;
import com.my.Ejb.UserEJBLocal;
import com.my.Ejb.VolunteerBeanLocal;
import com.my.Entity.BasicInformationEntity;
import com.my.Entity.RecDomainWord;
import com.my.Entity.Register;
import static com.vol.util.RecordHelper.createTime;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

/**
 *
 * @author user
 */
@Named(value = "create_Register")
@SessionScoped
public class create_Register implements Serializable {

    @EJB
    VolunteerBeanLocal volunteerService;
    @EJB
    RegisterEJBLocal registerService;
    @EJB
    UserEJBLocal userService;
    @EJB
    OrgEJBLocal orgService;

    private BasicInformationEntity selectedVolunteer;

    private String idNumber;
    private String name;
    //紀錄冊字
    private String rec_domain_word;
    //紀錄冊中文編號
    private String rec_word;
    //紀錄冊號
    private String rec_code;
    //發冊日期
    private String confer_date;
    //發冊類別
    private String confer_type;
    private String memo;
    private String org_code;

    @PostConstruct
    public void init() {

    }

    /**
     * 設定畫面資料為預設
     */
    public void reset() {
        idNumber = "";
        rec_domain_word = "";
        rec_word = "";
        rec_code = "";
        confer_date = "";
        confer_type = "";
        memo = "";
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getOrg_code() {
        return org_code;
    }

    public void setOrg_code(String org_code) {
        this.org_code = org_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRec_domain_word() {
        return rec_domain_word;
    }

    public void setRec_domain_word(String rec_domain_word) {
        this.rec_domain_word = rec_domain_word;
    }

    public String getRec_word() {
        return rec_word;
    }

    public void setRec_word(String rec_word) {
        this.rec_word = rec_word;
    }

    public String getRec_code() {
        return rec_code;
    }

    public void setRec_code(String rec_code) {
        this.rec_code = rec_code;
    }

    public String getConfer_date() {
        return confer_date;
    }

    public void setConfer_date(String confer_date) {
        this.confer_date = confer_date;
    }

    public String getConfer_type() {
        return confer_type;
    }

    public void setConfer_type(String confer_type) {
        this.confer_type = confer_type;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public create_Register() {
    }

    /**
     * 取得使用者所選的志工姓名
     * @param e 
     */
    public void selectedVolunteer(ValueChangeEvent e) {
        this.name = volunteerService.findVolunteer(e.getNewValue().toString()).getName();
    }

    public void String(BasicInformationEntity selectedVolunteer) {
        this.selectedVolunteer = selectedVolunteer;
    }

    /**
     * 取得所有字第的選項清單
     * @return 
     */
    public List<SelectItem> getAllRecDomainWord() {
        List<SelectItem> setList = new ArrayList<SelectItem>();
        setList.add(new SelectItem("", ""));
        for (RecDomainWord id : (List<RecDomainWord>) registerService.allRecDomainWord()) {
            setList.add(new SelectItem(id.getR_no(), id.getRecDomainWord()));
        }
        return setList;
    }

    /**
     * 取得使用者選取的志工隊下之志工名單
     * @return 
     */
    public List<SelectItem> getAllVolunteerItems() {
        List<SelectItem> setList = new ArrayList<SelectItem>();
        for (BasicInformationEntity id : volunteerService.allVolunteers_ByOrgCode(org_code)) {
            setList.add(new SelectItem(id.getIdNumber(), id.getName()));
        }
        return setList;
    }
    
    /**
     * 將此筆紀錄冊資料儲存至資料庫
     * @return 
     */
    public String save() {
        //設定參數
        Register r = new Register();
        r.setIdNumber(idNumber);
        r.setName(name);
        r.setRec_domain_word(rec_domain_word);
        r.setRec_word(rec_word);
        r.setRec_code(rec_code);
        r.setConfer_date(confer_date);
        r.setConfer_type(confer_type);
        r.setMemo(memo);
        r.setOrg_code(org_code);
        r.setCreate_time(createTime());
        //存入資料庫
        registerService.addRegister(r);
        return "search_Register";
    }

}
