/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vol.bean.register_book;

import com.my.Ejb.RegisterEJBLocal;
import com.my.Ejb.VolunteerBeanLocal;
import com.my.Entity.BasicInformationEntity;
import com.my.Entity.RecDomainWord;
import com.my.Entity.Register;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author user
 */
@Named(value = "update_R")
@SessionScoped
public class update_Register implements Serializable {

    @EJB
    VolunteerBeanLocal volunteerService;
    @EJB
    RegisterEJBLocal registerService;

    private Long id;
    private String idNumber;
    private String name;
    private String rec_domain_word;
    private String rec_word;
    private String rec_code;
    private String confer_date;
    private String confer_type;
    private String memo;

    //設定畫面資料為預設
    public void reset() {
        rec_domain_word = "";
        rec_word = "";
        rec_code = "";
        confer_date = "";
        confer_type = "";
        memo = "";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRec_domain_word() {
        return rec_domain_word;
    }

    public void setRec_domain_word(String rec_domain_word) {
        this.rec_domain_word = rec_domain_word;
    }

    public String getRec_word() {
        return rec_word;
    }

    public void setRec_word(String rec_word) {
        this.rec_word = rec_word;
    }

    public String getRec_code() {
        return rec_code;
    }

    public void setRec_code(String rec_code) {
        this.rec_code = rec_code;
    }

    public String getConfer_date() {
        return confer_date;
    }

    public void setConfer_date(String confer_date) {
        this.confer_date = confer_date;
    }

    public String getConfer_type() {
        return confer_type;
    }

    public void setConfer_type(String confer_type) {
        this.confer_type = confer_type;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public update_Register() {
    }

    //取得所有字第的選項清單
    public List<SelectItem> getAllRecDomainWord() {
        List<SelectItem> setList = new ArrayList<SelectItem>();
        setList.add(new SelectItem("", ""));
        for (RecDomainWord id : (List<RecDomainWord>) registerService.allRecDomainWord()) {
            setList.add(new SelectItem(id.getR_no(), id.getRecDomainWord()));
        }
        return setList;
    }

    Register r;

    //設定使用者要查看的該筆紀錄冊資料

    public String ViewR() {
        idNumber = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("idNumber");
        id = (Long) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("id");
        r = registerService.findRegister(idNumber, id);
        setName(r.getName());
        setIdNumber(r.getIdNumber());
        setRec_domain_word(r.getRec_domain_word());
        setRec_word(r.getRec_word());
        setRec_code(r.getRec_code());
        setConfer_date(r.getConfer_date());
        setConfer_type(r.getConfer_type());
        setMemo(r.getMemo());

        return "update_Register.xhtml";
    }

    //更新教育訓練資料
    public String update() {
        //設定參數
        Register r = new Register();
        r.setId(id);
        r.setIdNumber(idNumber);
        r.setName(name);
        r.setRec_domain_word(rec_domain_word);
        r.setRec_word(rec_word);
        r.setRec_code(rec_code);
        r.setConfer_date(confer_date);
        r.setConfer_type(confer_type);
        r.setMemo(memo);
        registerService.updateRegister(r);
        return "search_Register";
    }

}
