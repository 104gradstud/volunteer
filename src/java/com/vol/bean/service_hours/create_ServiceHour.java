/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vol.bean.service_hours;

import com.my.Ejb.OrgEJBLocal;
import com.my.Ejb.ServiceHoursEJBLocal;
import com.my.Ejb.UserEJBLocal;
import com.my.Ejb.VolunteerBeanLocal;
import com.my.Entity.BasicInformationEntity;
import com.my.Entity.ServiceHours;
import static com.vol.util.RecordHelper.createTime;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorListener;
import org.primefaces.context.RequestContext;

/**
 *
 * @author user
 */
@Named(value = "create_ServiceHour")
@SessionScoped
public class create_ServiceHour implements Serializable {

    @EJB
    VolunteerBeanLocal volunteerService;
    @EJB
    ServiceHoursEJBLocal serviceHoursService;
    @EJB
    UserEJBLocal userService;
    @EJB
    OrgEJBLocal orgService;

    /**
     * 紀錄被選取要加入服務時數的志工
     */
    private List<BasicInformationEntity> selectedItems;

    private String idNumber;
    private String Name;
    private String service_date_s;
    private String service_date_e;
    private String service_item;
    private String service_content;
    private String[] service_area;
    private String memo;
    /**
     * 受服務人次
     */
    private String[] NumberOfServicePeople;
    /**
     * 批次輸入所依據的受服務人次欄位
     */
    private String NumberOfServicePeople_Main;
    private String[] hour;
    /**
     * 批次輸入所依據的小時欄位
     */
    private String hour_Main;
    private String[] min;
    /**
     * 批次輸入所依據的分鐘欄位
     */
    private String min_Main;
    private String[] TransportationExpenses;
    /**
     * 批次輸入所依據的交通費欄位
     */
    private String TransportationExpenses_Main;
    private String[] MealCosts;
    /**
     * 批次輸入所依據的膳費欄位
     */
    private String MealCosts_Main;
    private String entry;
    private String org_code;

    //預設值
    @PostConstruct
    void init() {
        entry = "batch";//設為批次輸入
    }

    /**
     * 取得使用者選取的志工隊下之志工名單
     *
     * @return
     */
    public List<BasicInformationEntity> getAllVolunteerItems() {
        return volunteerService.allVolunteers_ByOrgCode(org_code);
    }

    /**
     * 設定畫面資料為預設
     *
     */
    public void reset() {
        service_date_s = null;
        service_date_e = null;
        selectedItems = null;
        entry = "batch";
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getService_date_s() {
        return service_date_s;
    }

    public void setService_date_s(String service_date_s) {
        this.service_date_s = service_date_s;
    }

    public String getService_date_e() {
        return service_date_e;
    }

    public void setService_date_e(String service_date_e) {
        this.service_date_e = service_date_e;
    }

    public String getService_item() {
        return service_item;
    }

    public void setService_item(String service_item) {
        this.service_item = service_item;
    }

    public String getService_content() {
        return service_content;
    }

    public void setService_content(String service_content) {
        this.service_content = service_content;
    }

    public String[] getService_area() {
        return service_area;
    }

    public void setService_area(String[] service_area) {
        this.service_area = service_area;
    }

    public String[] getNumberOfServicePeople() {
        return NumberOfServicePeople;
    }

    public void setNumberOfServicePeople(String[] NumberOfServicePeople) {
        this.NumberOfServicePeople = NumberOfServicePeople;
    }

    public String getNumberOfServicePeople_Main() {
        return NumberOfServicePeople_Main;
    }

    public void setNumberOfServicePeople_Main(String NumberOfServicePeople_Main) {
        this.NumberOfServicePeople_Main = NumberOfServicePeople_Main;
    }

    public String[] getHour() {
        return hour;
    }

    public void setHour(String[] hour) {
        this.hour = hour;
    }

    public String getHour_Main() {
        return hour_Main;
    }

    public void setHour_Main(String hour_Main) {
        this.hour_Main = hour_Main;
    }

    public String[] getMin() {
        return min;
    }

    public void setMin(String[] min) {
        this.min = min;
    }

    public String getMin_Main() {
        return min_Main;
    }

    public void setMin_Main(String min_Main) {
        this.min_Main = min_Main;
    }

    public String[] getTransportationExpenses() {
        return TransportationExpenses;
    }

    public void setTransportationExpenses(String[] TransportationExpenses) {
        this.TransportationExpenses = TransportationExpenses;
    }

    public String getTransportationExpenses_Main() {
        return TransportationExpenses_Main;
    }

    public void setTransportationExpenses_Main(String TransportationExpenses_Main) {
        this.TransportationExpenses_Main = TransportationExpenses_Main;
    }

    public String[] getMealCosts() {
        return MealCosts;
    }

    public void setMealCosts(String[] MealCosts) {
        this.MealCosts = MealCosts;
    }

    public String getMealCosts_Main() {
        return MealCosts_Main;
    }

    public void setMealCosts_Main(String MealCosts_Main) {
        this.MealCosts_Main = MealCosts_Main;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getEntry() {
        return entry;
    }

    public void setEntry(String entry) {
        this.entry = entry;
    }

    public String getOrg_code() {
        return org_code;
    }

    public void setOrg_code(String org_code) {
        this.org_code = org_code;
    }

    public List<BasicInformationEntity> getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(List<BasicInformationEntity> selectedItems) {
        this.selectedItems = selectedItems;
    }

    public create_ServiceHour() {
    }

    /**
     * 批次與分別輸入的Ajax Listener設定
     *
     * @param event
     */
    public void batchAjax(AjaxBehaviorListener event) {
        //若使用者點選批次輸入，將所有所有選取的志工資料(核定日期、核定名稱)更新為與上方批次輸入的欄位相同。
        if (entry.equals("batch")) {
            try {
                for (int i = 0; i < NumberOfServicePeople.length; i++) {
                    NumberOfServicePeople[i] = getNumberOfServicePeople_Main();
                    hour[i] = getHour_Main();
                    min[i] = getMin_Main();
                    TransportationExpenses[i] = getTransportationExpenses_Main();
                    MealCosts[i] = getMealCosts_Main();
                }
            } catch (NullPointerException e) {
            }
        }
    }

    /**
     * 執行儲存。 判斷有無必填欄位未填，或是輸入資料有誤，將回傳錯誤訊息。 若沒問題，將時數平均到每個月份中儲存。
     *
     * @return
     * @throws java.text.ParseException
     */
    public String save() throws ParseException {
        //找出必填  卻未填的欄位  (因為在頁面上設置required="true"，Dialog跳不出來)
        String[] required = new String[4];
        required[0] = service_date_s;
        required[1] = service_date_e;
        required[2] = service_item;
        required[3] = service_content;
        boolean nu = false;
        boolean er = false;
        //判斷有無填寫服務時數資料
        int year_s = 0;
        int year_e = 999999999;
        for (int i = 0; i < required.length; i++) {
            if (required[i] == null || required[i].length() == 0) {
                nu = true;
            } else {
                if (i == 0 || i == 1) {
                    String[] y_part = required[i].split("-");
                    int year = Integer.parseInt(y_part[0] + y_part[1] + y_part[2]);
                    if (i == 0) {
                        year_s = year;
                    }
                    if (i == 1) {
                        year_e = year;
                    }
                    if (year < 20010120) {
                        er = true;
                        FacesContext.getCurrentInstance().addMessage("form:msg", new FacesMessage("訓綀日期起迄不可早於2001/1/20"));
                    }
                    if (year_e < year_s) {
                        er = true;
                        FacesContext.getCurrentInstance().addMessage("form:msg", new FacesMessage("訓綀日期迄不可早於起"));
                    }
                    if (y_part[0].length() > 4) {
                        er = true;
                        FacesContext.getCurrentInstance().addMessage("form:msg", new FacesMessage("年份格式有誤，請再確認"));
                    }
                }
            }
        }
        //判斷有無選擇志工、填寫欄位
        try {
            for (int i = 0; i < NumberOfServicePeople.length; i++) {
                if (NumberOfServicePeople[i] == null || NumberOfServicePeople[i].length() == 0 || hour[i] == null || hour[i].length() == 0 || min[i] == null || min[i].length() == 0 || TransportationExpenses == null || TransportationExpenses[i].length() == 0 || MealCosts[i] == null || MealCosts[i].length() == 0) {
                    nu = true;
                } else if (Integer.parseInt(hour[i]) < 1) {
                    FacesContext.getCurrentInstance().addMessage("form:msg", new FacesMessage("服務小時至少為1"));
                    er = true;
                }
            }
        } catch (NullPointerException e) {
            FacesContext.getCurrentInstance().addMessage("form:msg", new FacesMessage("請選擇至少一位志工"));
            er = true;
        }

        //顯示提示訊息
        if (nu || er) {
            if (nu) {
                FacesContext.getCurrentInstance().addMessage("form:msg", new FacesMessage("請輸入所有必填欄位！"));
            }
            return "create_HoursOfService.xhtml";
        } else {
            //將服務區域以單一字串格式儲存
            String Service_Area_String = "";
            for (int i = 0; i < service_area.length; i++) {
                Service_Area_String = Service_Area_String + service_area[i] + ",";
                if (i == service_area.length - 1) {
                    Service_Area_String = Service_Area_String.substring(0, Service_Area_String.length() - 1);
                }
            }
            //判斷有幾位志工，依序將每筆資料存入
            for (int i = 0; i < NumberOfServicePeople.length; i++) {
                idNumber = selectedItems.get(i).getIdNumber();
                Name = selectedItems.get(i).getName();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                //取得兩個日期之間的日期陣列。
                //例如：20160103~20160315，會回傳[20160103~20160131]、[20160201~20160229]、[20160301~20160315]
                List eDate = getMonthBetween(sdf.parse(service_date_s), sdf.parse(service_date_e));
                String[] s = service_date_s.split("-");
                String[] e = service_date_e.split("-");
                //計算兩日期之間共有幾個月。計算方式：((End年-Start年)*12 + End月-Start月) + 1
                int M = ((Integer.parseInt(e[0]) - Integer.parseInt(s[0])) * 12 + Integer.parseInt(e[1]) - Integer.parseInt(s[1])) + 1;
                //將小時數平均到每個月，若有餘數放入MOD
                int Average_H = Integer.parseInt(hour[i]) / M;
                int mod = Integer.parseInt(hour[i]) % M;
                //設定小時h、服務日期起s_d_s、服務日期迄s_d_e陣列數量與月份相同。
                int[] h = new int[M];
                String[] s_d_s = new String[M];
                String[] s_d_e = new String[M];

                //從第一個月開始存
                for (int j = 0; j < M; j++) {
                    //將日設為01
                    s_d_s[j] = eDate.get(j).toString().substring(0, 7) + "-01";
                    s_d_e[j] = eDate.get(j).toString();
                    if (j == 0) {
                        //若是第一筆日期，設定為使用者輸入的起始日期
                        s_d_s[j] = service_date_s;
                    }

                    ServiceHours sh = new ServiceHours();
                    //設置參數
                    sh.setIdNumber(idNumber);
                    sh.setName(Name);
                    sh.setService_date_s(s_d_s[j]);
                    sh.setService_date_e(s_d_e[j]);
                    sh.setService_item(service_item);
                    sh.setService_content(service_content);
                    sh.setService_area(Service_Area_String);
                    sh.setMemo(memo);
                    sh.setNumberOfServicePeople(NumberOfServicePeople[i]);
                    sh.setTransportationExpenses(TransportationExpenses[i]);
                    sh.setMealCosts(MealCosts[i]);
                    sh.setOrg_code(org_code);
                    sh.setCreate_time(createTime());

                    //比對目前迴圈中的日期j與結束日期是否為同一月份，若為true，設定s_d_e為service_date_e，且將小時的餘數、分鐘也儲存。
                    if (eDate.get(j).toString().substring(0, 7).equals(service_date_e.substring(0, 7))) {
                        h[j] = Average_H + mod;
                        s_d_e[j] = service_date_e;
                        sh.setHours(String.valueOf(h[j]));
                        sh.setService_date_e(s_d_e[j]);
                        sh.setMins(min[i]);
                        //存入資料庫
                        serviceHoursService.addServiceHours(sh);
                        break;
                    }
                    if (Average_H >= 1) {
                        //否則就存入平均的小時，設定分鐘為0  
                        h[j] = Average_H;
                        sh.setHours(String.valueOf(h[j]));
                        sh.setMins("0");
                        //存入資料庫
                        serviceHoursService.addServiceHours(sh);
                    }
                }
            }
            return "search_HoursOfService";
        }
    }

    /**
     * 取得指定月份的最大天數
     *
     * @param year
     * @param month
     * @return
     */
    public static int getDaysByYearMonth(int year, int month) {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.YEAR, year);
        a.set(Calendar.MONTH, month);
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        int maxDate = a.get(Calendar.DATE);
        return maxDate;
    }

    /**
     * 取得兩個日期之間的日期陣列。
     * 例如：20160103~20160315，會回傳[20160103~20160131]、[20160201~20160229]、[20160301~20160315]
     *
     * @param minDate
     * @param maxDate
     * @return
     */
    public List<String> getMonthBetween(Date minDate, Date maxDate) {
        ArrayList<String> result = new ArrayList<String>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");

        Calendar min = Calendar.getInstance();
        Calendar max = Calendar.getInstance();
        //起始日期
        min.setTime(minDate);
        min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);
        //結束日期
        max.setTime(maxDate);
        max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH), 2);

        Calendar curr = min;
        while (curr.before(max)) {
            //取得指定月份的最大天數
            int MaxDaysOfMonth = getDaysByYearMonth(curr.get(Calendar.YEAR), curr.get(Calendar.MONTH));
            result.add(sdf.format(curr.getTime()) + "-" + MaxDaysOfMonth);
            curr.add(Calendar.MONTH, 1);
        }
        return result;
    }

    /**
     * 開啟selectVolunteers_SH對話框，供使用者選取志工
     */
    public void chooseVolunteers() {
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("draggable", true);
        RequestContext.getCurrentInstance().openDialog("selectVolunteers_SH", options, null);
    }

    /**
     * 設定受服務人次、小時、分鐘、交通費、膳費的陣列數量
     */
    public void onVolunteersChosen() {
        NumberOfServicePeople = new String[selectedItems.size()];
        hour = new String[selectedItems.size()];
        min = new String[selectedItems.size()];
        TransportationExpenses = new String[selectedItems.size()];
        MealCosts = new String[selectedItems.size()];
    }

    /**
     * 關閉selectVolunteers_SH對話框
     */
    public void closeDialog() {
        RequestContext.getCurrentInstance().closeDialog("selectVolunteers_SH");
    }

}
