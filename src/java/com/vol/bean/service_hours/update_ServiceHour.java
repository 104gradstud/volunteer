/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vol.bean.service_hours;

import com.my.Ejb.ServiceHoursEJBLocal;
import com.my.Ejb.VolunteerBeanLocal;
import com.my.Entity.EducationTraining;
import com.my.Entity.ServiceHours;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.sql.Date;

import javax.ejb.EJB;
import javax.faces.context.FacesContext;

/**
 *
 * @author user
 */
@Named(value = "update_SH")
@SessionScoped
public class update_ServiceHour implements Serializable {

    @EJB
//    VolunteerBeanLocal volunteerService;
    ServiceHoursEJBLocal serviceHoursService;
    ServiceHours sh;

    private Long id;
    private String idNumber;
    private String Name;
    private String service_date_s;
    private String service_date_e;
    private String service_item;
    private String service_content;
    private String[] service_area;
    private String memo;
    private String NumberOfServicePeople;
    private String hours;
    private String mins;
    private String TransportationExpenses;
    private String MealCosts;
    private String org;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getMins() {
        return mins;
    }

    public void setMins(String mins) {
        this.mins = mins;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getService_date_s() {
        return service_date_s;
    }

    public void setService_date_s(String service_date_s) {
        this.service_date_s = service_date_s;
    }

    public String getService_date_e() {
        return service_date_e;
    }

    public void setService_date_e(String service_date_e) {
        this.service_date_e = service_date_e;
    }

    public String getService_item() {
        return service_item;
    }

    public void setService_item(String service_item) {
        this.service_item = service_item;
    }

    public String getService_content() {
        return service_content;
    }

    public void setService_content(String service_content) {
        this.service_content = service_content;
    }

    public String[] getService_area() {
        return service_area;
    }

    public void setService_area(String[] service_area) {
        this.service_area = service_area;
    }

    public String getNumberOfServicePeople() {
        return NumberOfServicePeople;
    }

    public void setNumberOfServicePeople(String NumberOfServicePeople) {
        this.NumberOfServicePeople = NumberOfServicePeople;
    }

    public String getTransportationExpenses() {
        return TransportationExpenses;
    }

    public void setTransportationExpenses(String TransportationExpenses) {
        this.TransportationExpenses = TransportationExpenses;
    }

    public String getMealCosts() {
        return MealCosts;
    }

    public void setMealCosts(String MealCosts) {
        this.MealCosts = MealCosts;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    /**
     * Creates a new instance of create
     */
    public update_ServiceHour() {
    }

    //設定使用者要查看的該筆服務時數資料
    public String ViewSH() {
        idNumber = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("idNumber");
        id = (Long) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("id");
        sh = serviceHoursService.findServiceHours(idNumber, id);
        setName(sh.getName());
        setIdNumber(sh.getIdNumber());
        setService_date_s(sh.getService_date_s());
        setService_date_e(sh.getService_date_e());
        setService_item(sh.getService_item());
        setService_content(sh.getService_content());
        if (sh.getService_area() != null) {
            String[] S_Items = sh.getService_area().split(",");
            setService_area(S_Items);
        }
        setMemo(sh.getMemo());
        setNumberOfServicePeople(sh.getNumberOfServicePeople());
        setHours(sh.getHours());
        setMins(sh.getMins());
        setTransportationExpenses(sh.getTransportationExpenses());
        setMealCosts(sh.getMealCosts());

        return "update_HoursOfService.xhtml";
    }

    /**
     * 更新服務時數資料
     * @return 
     */
    public String update() {
        //將服務地區Service_Items轉成字串
        String Service_Areas_String = "";
        for (int i = 0; i < service_area.length; i++) {
            Service_Areas_String = Service_Areas_String + service_area[i] + ",";
            if (i == service_area.length - 1) {
                Service_Areas_String = Service_Areas_String.substring(0, Service_Areas_String.length() - 1);
            }
        }
        //設置參數，準備儲存
        ServiceHours sh = new ServiceHours();
        sh.setId(id);
        sh.setIdNumber(idNumber);
        sh.setName(Name);
        sh.setService_date_s(service_date_s);
        sh.setService_date_e(service_date_e);
        sh.setService_item(service_item);
        sh.setService_content(service_content);
        sh.setService_area(Service_Areas_String);
        sh.setMemo(memo);
        sh.setNumberOfServicePeople(NumberOfServicePeople);
        sh.setHours(hours);
        sh.setMins(mins);
        sh.setTransportationExpenses(TransportationExpenses);
        sh.setMealCosts(MealCosts);
        //存入資料庫
        serviceHoursService.updateSH(sh);
        return "search_HoursOfService";
    }

}
