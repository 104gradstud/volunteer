/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vol.bean.user;

import com.my.Ejb.OrgEJBLocal;
import com.my.Ejb.UserEJBLocal;
import com.my.Entity.Org;
import com.my.Entity.Users;
import com.my.Entity.Users_Groups;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author user
 */
@Named(value = "create_User")
@SessionScoped
public class create_User implements Serializable {

    @EJB
    UserEJBLocal userService;
    @EJB
    OrgEJBLocal orgService;

    private String account;
    private String password;
    private String password_confirm;
    //紀錄組織代碼
    private String orgCode;
    //紀錄父階層代碼
    private String parentCode;
    private String groupID;
    //紀錄使用者要加入的階層。例如：運用單位代碼：3；志工隊代碼：4
    private String layer = "1";
    /**
     * 地方主管機關欄位編號
     */
    private String selectedOrg_Code1Layer;
    /**
     * 目的事業主管機關欄位編號
     */
    private String selectedOrg_Code2Layer;
    /**
     * 運用單位欄位編號
     */
    private String selectedOrg_Code3Layer;
    /**
     * 志工隊欄位編號
     */
    private String selectedOrg_Code4Layer;

    @PostConstruct
    public void init() {

    }

    //設定畫面資料為預設
    public void reset() {
        account = "";
        password = "";
        password_confirm = "";
        orgCode = "";
        parentCode = "";
        groupID = "";
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword_confirm() {
        return password_confirm;
    }

    public void setPassword_confirm(String password_confirm) {
        this.password_confirm = password_confirm;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    //回傳階層代碼
    public int layerCode() {
        return Integer.parseInt(layer);
    }

    public String getLayer() {
        return layer;
    }

    public void setLayer(String layer) {
        this.layer = layer;
    }

    public String getSelectedOrg_Code1Layer() {
        return selectedOrg_Code1Layer;
    }

    public void setSelectedOrg_Code1Layer(String selectedOrg_Code1Layer) {
        this.selectedOrg_Code1Layer = selectedOrg_Code1Layer;
    }

    public String getSelectedOrg_Code2Layer() {
        return selectedOrg_Code2Layer;
    }

    public void setSelectedOrg_Code2Layer(String selectedOrg_Code2Layer) {
        this.selectedOrg_Code2Layer = selectedOrg_Code2Layer;
    }

    public String getSelectedOrg_Code3Layer() {
        return selectedOrg_Code3Layer;
    }

    public void setSelectedOrg_Code3Layer(String selectedOrg_Code3Layer) {
        this.selectedOrg_Code3Layer = selectedOrg_Code3Layer;
    }

    public String getSelectedOrg_Code4Layer() {
        return selectedOrg_Code4Layer;
    }

    public void setSelectedOrg_Code4Layer(String selectedOrg_Code4Layer) {
        this.selectedOrg_Code4Layer = selectedOrg_Code4Layer;
    }

    public create_User() {
    }

    //取得地方主管機關單位清單，並回傳
    public List<SelectItem> getSpecific_Org1Layer() {
        List<SelectItem> setList = new ArrayList<SelectItem>();
        setList.add(new SelectItem("", "[地方主管機關]"));
        for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode("0")) {
            setList.add(new SelectItem(id.getOrgCode(), id.getOrgName()));
        }
        return setList;
    }

    //取得特定的目的事業主管機關單位清單，並回傳
    public List<SelectItem> getSpecific_Org2Layer() {
        List<SelectItem> setList = new ArrayList<SelectItem>();
        setList.add(new SelectItem("", "[目的事業主管機關]"));
        for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode(selectedOrg_Code1Layer)) {
            setList.add(new SelectItem(id.getOrgCode(), id.getOrgName()));
        }
        return setList;
    }

    //取得特定的運用單位清單，並回傳
    public List<SelectItem> getSpecific_Org3Layer() {
        List<SelectItem> setList = new ArrayList<SelectItem>();
        setList.add(new SelectItem("", "[運用單位]"));
        for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode(selectedOrg_Code2Layer)) {
            setList.add(new SelectItem(id.getOrgCode(), id.getOrgName()));
        }
        return setList;
    }

    //取得特定的志工隊，並回傳
    public List<SelectItem> getSpecific_Org4Layer() {
        List<SelectItem> setList = new ArrayList<SelectItem>();
        setList.add(new SelectItem("", "[志工隊]"));
        for (Org id : (List<Org>) orgService.findSpecific_OrgByParentCode(selectedOrg_Code3Layer)) {
            setList.add(new SelectItem(id.getOrgCode(), id.getOrgName()));
        }
        return setList;
    }

    //儲存此筆使用者資料
    public String save() {
        if (getPassword_confirm().equals(getPassword())) {
            //判斷使用者加入的組織階層，記錄組織代碼與父層代碼
            switch (layerCode()) {
                case 1:
                    parentCode = "0";
                    orgCode = selectedOrg_Code1Layer;
                    break;
                case 2:
                    parentCode = selectedOrg_Code1Layer;
                    orgCode = selectedOrg_Code2Layer;
                    break;
                case 3:
                    parentCode = selectedOrg_Code2Layer;
                    orgCode = selectedOrg_Code3Layer;
                    break;
                case 4:
                    parentCode = selectedOrg_Code3Layer;
                    orgCode = selectedOrg_Code4Layer;
                    break;
            }
            //設定參數
            Users u = new Users();
            u.setAccount(account);
            u.setPassword(password);
            u.setOrgCode(orgCode);
            u.setParentCode(parentCode);
            Users_Groups ug = new Users_Groups();
            ug.setAccount(account);
            ug.setGroupID(groupID);
            //存入資料庫
            userService.addUser(u, ug);
            return "search_User";
        } else {
            String msg = "兩次輸入的密碼不相同，請再確認!";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(msg));
            return "create_user";
        }
    }

    //確認兩次輸入密碼是否相同。
//    public void password_confirm(AjaxBehaviorListener event) {
//        if (getPassword_confirm().equals(getPassword())) {
//        } else {
//            String msg = "兩次輸入的密碼不相同，請再確認!";
//            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(msg));
//        }
//    }
}
