/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vol.bean;

import com.my.Ejb.VolunteerBeanLocal;
import com.my.Entity.BasicInformationEntity;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;

/**
 * @deprecated 
 * @author user
 */
@Named(value = "volBean")
@SessionScoped
public class volBean implements Serializable {

    /**
     * Creates a new instance of volBean
     */
    @EJB
    VolunteerBeanLocal volunteerService;
    String Name;
    String idNumber;
    List<BasicInformationEntity> viewVol;

    public volBean() {
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }
    //呼叫EJB中的createVolunteer方法，將值存入資料庫。
    public String createVol() {
        volunteerService.createVolunteer(idNumber, Name);
        return null;
    }
    //顯示result頁面。
    public String result() {
        return "result";
    }
    //取得資料庫中所有資料。
    public List<BasicInformationEntity> getViewVol() {
        return volunteerService.viewVolunteer();
    }
}
