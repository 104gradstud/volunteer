/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vol.bean.volunteer;

import com.my.Ejb.OrgEJBLocal;
import com.my.Ejb.UserEJBLocal;
import com.my.Ejb.VolunteerBeanLocal;
import com.my.Entity.Area;
import com.my.Entity.BasicInformationEntity;
import com.my.Entity.City;
import com.my.Entity.Specialty;
import com.my.Entity.Village;
import static com.vol.util.RecordHelper.createTime;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.security.DeclareRoles;
import javax.annotation.security.DenyAll;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

/**
 *
 * @author user
 */
@Named(value = "create_volunteer")
@SessionScoped
public class create_volunteer implements Serializable {

    @EJB
    VolunteerBeanLocal volunteerService;
    @EJB
    UserEJBLocal userService;
    @EJB
    OrgEJBLocal orgService;

    private String idNumber;
    private String Name;
    private String Nationality;
    private String Detail_Nationality;
    /**
     * 其他國籍說明
     */
    private String Other_Detail_Nationality;
    /**
     * 民國前 欄位
     */
    private Boolean Republic;
    private String Birthday;
    private String Sex;
    private Boolean Aboriginal;
    private String[] Service_Items;
    private String Marriage;
    private String Specialty;
    private String Specialty2;
    private String Specialty3;
    private String Password;
    private String Password_Confirm;
    /**
     * 是否加入救災志工
     */
    private Boolean Disaster_Relief;
    private String Occupation;
    private String Educational_Background;
    private String E_mail;
    /**
     * 戶籍郵遞區號
     */
    private String Address_Residence_Postal_Code;
    private String Address_Residence_City;
    private String Address_Residence_Area;
    private String Address_Residence_Village;
    private String Address_Residence_Neighborhood = "";
    private String Address_Residence_Road_No;
    private String Address_Mailing;
    private String Contact_Phone;
    private String Phone;
    private String Contact_Phone2;
    private String Phone2;
    private String Self_Introduction;
    /**
     * 最早服務起始日
     */
    private String The_Earliest_starting_Date_Service;
    private String Remark;
    /**
     * 收編狀態 欄位
     */
    private String Incorporation;
    private String Join_Date;
    private String Volunteer_ID;
    private String Volunteer_Title;
    /**
     * 志工類別
     */
    private String Volunteer_Identity;
    /**
     * 可服務開始日期
     */
    private String Serviceability_Start_Date;
    /**
     * 可服務結束日期
     */
    private String Serviceability_End_Date;
    /**
     * 可服務時段
     */
    private String[] Serviceability_Times;
    /**
     * 密碼錯誤訊息提示
     *
     * @deprecated
     */
    private String checkMsg;
    /**
     * City ID 縣市編號
     */
    private String city_code;
    /**
     * Area code 鄉鎮編號
     */
    private String area_code;
    /**
     * Village code 村里編號
     */
    private String village_code;
    private String city_name;
    private String area_name;
    private String village_name;
    /**
     * 同戶籍checkBox
     */
    private Boolean SameWithResidence;

    /**
     * 志工隊欄位編號
     */
    private String org_Code;

    //預設值
    @PostConstruct
    void init() {
        Disaster_Relief = Boolean.FALSE;
        Nationality = "1";
    }

    public Boolean getSameWithResidence() {
        return SameWithResidence;
    }

    public void setSameWithResidence(Boolean SameWithResidence) {
        this.SameWithResidence = SameWithResidence;
    }

    public String getCity_code() {
        return city_code;
    }

    public void setCity_code(String city_code) {
        this.city_code = city_code;
    }

    public String getArea_code() {
        return area_code;
    }

    public void setArea_code(String area_code) {
        this.area_code = area_code;
    }

    public String getVillage_code() {
        return village_code;
    }

    public void setVillage_code(String village_code) {
        this.village_code = village_code;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getArea_name() {
        return area_name;
    }

    public void setArea_name(String area_name) {
        this.area_name = area_name;
    }

    public String getVillage_name() {
        return village_name;
    }

    public void setVillage_name(String village_name) {
        this.village_name = village_name;
    }

    public String getCheckMsg() {
        return checkMsg;
    }

    public void setCheckMsg(String checkMsg) {
        this.checkMsg = checkMsg;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String Nationality) {
        this.Nationality = Nationality;
    }

    public String getDetail_Nationality() {
        return Detail_Nationality;
    }

    public void setDetail_Nationality(String Detail_Nationality) {
        this.Detail_Nationality = Detail_Nationality;
    }

    public String getOther_Detail_Nationality() {
        return Other_Detail_Nationality;
    }

    public void setOther_Detail_Nationality(String Other_Detail_Nationality) {
        this.Other_Detail_Nationality = Other_Detail_Nationality;
    }

    public Boolean getAboriginal() {
        return Aboriginal;
    }

    public void setAboriginal(Boolean Aboriginal) {
        this.Aboriginal = Aboriginal;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getPassword_Confirm() {
        return Password_Confirm;
    }

    public void setPassword_Confirm(String Password_Confirm) {
        this.Password_Confirm = Password_Confirm;
    }

    public void default_password() {
        setPassword(idNumber);
        setPassword_Confirm(idNumber);
        setName(idNumber);
    }

    public Boolean getRepublic() {
        return Republic;
    }

    public void setRepublic(Boolean Republic) {
        this.Republic = Republic;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String Birthday) {
        this.Birthday = Birthday;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String Sex) {
        this.Sex = Sex;
    }

    public String getE_mail() {
        return E_mail;
    }

    public void setE_mail(String E_mail) {
        this.E_mail = E_mail;
    }

    public Boolean getDisaster_Relief() {
        return Disaster_Relief;
    }

    public void setDisaster_Relief(Boolean Disaster_Relief) {
        this.Disaster_Relief = Disaster_Relief;
    }

    public String getOccupation() {
        return Occupation;
    }

    public void setOccupation(String Occupation) {
        this.Occupation = Occupation;
    }

    public String getEducational_Background() {
        return Educational_Background;
    }

    public void setEducational_Background(String Educational_Background) {
        this.Educational_Background = Educational_Background;
    }

    public String getSpecialty() {
        return Specialty;
    }

    public void setSpecialty(String Specialty) {
        this.Specialty = Specialty;
    }

    public String getMarriage() {
        return Marriage;
    }

    public void setMarriage(String Marriage) {
        this.Marriage = Marriage;
    }

    public String getAddress_Residence_Postal_Code() {
        return Address_Residence_Postal_Code;
    }

    public void setAddress_Residence_Postal_Code(String Address_Residence_Postal_Code) {
        this.Address_Residence_Postal_Code = Address_Residence_Postal_Code;
    }

    public String getAddress_Residence_City() {
        return Address_Residence_City;
    }

    public void setAddress_Residence_City(String Address_Residence_City) {
        this.Address_Residence_City = Address_Residence_City;
    }

    public String getAddress_Residence_Area() {
        return Address_Residence_Area;
    }

    public void setAddress_Residence_Area(String Address_Residence_Area) {
        this.Address_Residence_Area = Address_Residence_Area;
    }

    public String getAddress_Residence_Village() {
        return Address_Residence_Village;
    }

    public void setAddress_Residence_Village(String Address_Residence_Village) {
        this.Address_Residence_Village = Address_Residence_Village;
    }

    public String getAddress_Residence_Neighborhood() {
        return Address_Residence_Neighborhood;
    }

    public void setAddress_Residence_Neighborhood(String Address_Residence_Neighborhood) {
        this.Address_Residence_Neighborhood = Address_Residence_Neighborhood;
    }

    public String getAddress_Residence_Road_No() {
        return Address_Residence_Road_No;
    }

    public void setAddress_Residence_Road_No(String Address_Residence_Road_No) {
        this.Address_Residence_Road_No = Address_Residence_Road_No;
    }

    public String getAddress_Mailing() {
        return Address_Mailing;
    }

    public void setAddress_Mailing(String Address_Mailing) {
        this.Address_Mailing = Address_Mailing;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    public String getPhone2() {
        return Phone2;
    }

    public void setPhone2(String Phone2) {
        this.Phone2 = Phone2;
    }

    public String getSelf_Introduction() {
        return Self_Introduction;
    }

    public void setSelf_Introduction(String Self_Introduction) {
        this.Self_Introduction = Self_Introduction;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String Remark) {
        this.Remark = Remark;
    }

    public String[] getService_Items() {
        return Service_Items;
    }

    public void setService_Items(String[] Service_Items) {
        this.Service_Items = Service_Items;
    }

    public String getSpecialty2() {
        return Specialty2;
    }

    public void setSpecialty2(String Specialty2) {
        this.Specialty2 = Specialty2;
    }

    public String getSpecialty3() {
        return Specialty3;
    }

    public void setSpecialty3(String Specialty3) {
        this.Specialty3 = Specialty3;
    }

    public String getContact_Phone() {
        return Contact_Phone;
    }

    public void setContact_Phone(String Contact_Phone) {
        this.Contact_Phone = Contact_Phone;
    }

    public String getContact_Phone2() {
        return Contact_Phone2;
    }

    public void setContact_Phone2(String Contact_Phone2) {
        this.Contact_Phone2 = Contact_Phone2;
    }

    public String getThe_Earliest_starting_Date_Service() {
        return The_Earliest_starting_Date_Service;
    }

    public void setThe_Earliest_starting_Date_Service(String The_Earliest_starting_Date_Service) {
        this.The_Earliest_starting_Date_Service = The_Earliest_starting_Date_Service;
    }

    public String getIncorporation() {
        return Incorporation;
    }

    public void setIncorporation(String Incorporation) {
        this.Incorporation = Incorporation;
    }

    public String getJoin_Date() {
        return Join_Date;
    }

    public void setJoin_Date(String Join_Date) {
        this.Join_Date = Join_Date;
    }

    public String getVolunteer_ID() {
        return Volunteer_ID;
    }

    public void setVolunteer_ID(String Volunteer_ID) {
        this.Volunteer_ID = Volunteer_ID;
    }

    public String getVolunteer_Title() {
        return Volunteer_Title;
    }

    public void setVolunteer_Title(String Volunteer_Title) {
        this.Volunteer_Title = Volunteer_Title;
    }

    public String getVolunteer_Identity() {
        return Volunteer_Identity;
    }

    public void setVolunteer_Identity(String Volunteer_Identity) {
        this.Volunteer_Identity = Volunteer_Identity;
    }

    public String getServiceability_Start_Date() {
        return Serviceability_Start_Date;
    }

    public void setServiceability_Start_Date(String Serviceability_Start_Date) {
        this.Serviceability_Start_Date = Serviceability_Start_Date;
    }

    public String getServiceability_End_Date() {
        return Serviceability_End_Date;
    }

    public void setServiceability_End_Date(String Serviceability_End_Date) {
        this.Serviceability_End_Date = Serviceability_End_Date;
    }

    public String[] getServiceability_Times() {
        return Serviceability_Times;
    }

    public void setServiceability_Times(String[] Serviceability_Times) {
        this.Serviceability_Times = Serviceability_Times;
    }

    public String getOrg_Code() {
        return org_Code;
    }

    public void setOrg_Code(String org_Code) {
        this.org_Code = org_Code;
    }

    /**
     * 當使用者選擇縣市後，取得縣市代碼
     *
     * @param event
     */
    public void selectChange_City(AjaxBehaviorListener event) {
        city_code = Address_Residence_City;
        if (city_code != null) {
            city_name = volunteerService.getCity_Value(city_code);
        }
    }

    /**
     * 當使用者選擇鄉鎮市區後，取得鄉鎮市區代碼
     *
     * @param event
     */
    public void selectChange_Area(AjaxBehaviorListener event) {
        area_code = Address_Residence_Area;
        if (area_code != null) {
            area_name = volunteerService.getArea_Value(area_code);
            Address_Residence_Postal_Code = volunteerService.getZipCode(area_code);
        }
    }

    /**
     * 當使用者選擇村里後，取得村里代碼
     *
     * @param event
     */
    public void selectChange_Village(AjaxBehaviorListener event) {
        village_code = Address_Residence_Village;
        if (village_code != null) {
            village_name = volunteerService.getVillage_Value(village_code);
        }
    }

    /**
     * 更改戶籍中的鄰欄位的 Event Listener
     *
     * @param event
     */
    public void Change_Neighborhood(ValueChangeEvent event) {
        Address_Residence_Neighborhood = ((String) event.getNewValue());
    }

    /**
     * 戶籍中路名欄位值的 value change event listener.
     *
     * @param event
     */
    public void Change_Road_No(ValueChangeEvent event) {
        Address_Residence_Road_No = ((String) event.getNewValue());
    }

    /**
     * 取得所有縣市的選項清單值
     *
     * @return {@code SelectItem} List
     * @see #volunteerService
     */
    public List<SelectItem> getAllCity() {
        List<SelectItem> setList = new ArrayList<SelectItem>();
        setList.add(new SelectItem("", "[縣 市]"));
        for (City id : (List<City>) volunteerService.allCity()) {
            setList.add(new SelectItem(id.getC_no(), id.getName()));
        }
        return setList;
    }

    /**
     * 取得特定的鄉鎮選項清單值。
     *
     * @return {@code SelectItem} List
     * @see #volunteerService
     */
    public List<SelectItem> getSpecific_Area() {
        List<SelectItem> setList = new ArrayList<SelectItem>();
        setList.add(new SelectItem("", "[鄉鎮市區]"));

        for (Area id : (List<Area>) volunteerService.specificArea(city_code)) {
            setList.add(new SelectItem(id.getA_no(), id.getName()));
        }
        return setList;
    }

    /**
     * 取得特定的村里選項清單值
     *
     * @return
     * @see #volunteerService
     */
    public List<SelectItem> getSpecific_Village() {
        List<SelectItem> setList = new ArrayList<SelectItem>();
        setList.add(new SelectItem("", "[村里]"));

        for (Village id : (List<Village>) volunteerService.specificVillage(area_code)) {
            setList.add(new SelectItem(id.getV_no(), id.getName()));
        }
        return setList;
    }

    /**
     * 取得所有專長的選項清單
     *
     * @return
     * @see #volunteerService
     */
    public List<SelectItem> getAllSpecialty() {
        List<SelectItem> setList = new ArrayList<SelectItem>();
        setList.add(new SelectItem("", ""));
        for (Specialty id : (List<Specialty>) volunteerService.allSpecialty()) {
            setList.add(new SelectItem(id.getS_no(), id.getSpecialty()));
        }
        return setList;
    }

    /**
     * 按下【同戶籍】按鈕後，觸發此方法， 完成後更新 {@link #Address_Mailing} 欄位。
     *
     * @param event
     */
    public void SetMailing(AjaxBehaviorListener event) {
        if (SameWithResidence) {
            String Neighborhood = "";
            if (getAddress_Residence_Neighborhood().length() != 0) {
                Neighborhood = "鄰";
            }
            String Residence = getAddress_Residence_Postal_Code()
                    + getCity_name()
                    + getArea_name()
                    + getVillage_name()
                    + getAddress_Residence_Neighborhood()
                    + Neighborhood
                    + getAddress_Residence_Road_No();
            setAddress_Mailing(Residence.replace("null", ""));
        } else {
            setAddress_Mailing("");
        }
    }

    public create_volunteer() {
    }

    /**
     * 檢查兩次密碼是否相同，相同的話進行再以下動作： 將部分boolean值、selectBox值轉成字串， 取得目前日期，儲存至資料庫。
     *
     * @return
     */
    @DenyAll
    public String save() {
        if (!getPassword_Confirm().equals(getPassword())) {
            String msg = "兩次輸入的密碼不相同，請再確認!";
            FacesContext.getCurrentInstance().addMessage("form:msg", new FacesMessage(msg));
            return "create_Volunteer.xhtml";
        } else {
            // 密碼和其確認密碼相同，進行儲存動作。
            String S_Aboriginal;
            //原住民 boolean 值轉字串
            //可以用 ?: 運算子(ternary operator)讓程式碼更精簡。
            S_Aboriginal = (Aboriginal) ? "Y" : "N";

            //ToDO：國民前 boolean 值轉字串
            String S_Republic;
            S_Republic = (Republic) ? "Y" : "N";

            //救災隊 boolean 值轉字串
            String S_Disaster_Relief;
            S_Disaster_Relief = (Disaster_Relief) ? "Y" : "N";

            if (Password.length() == 0) {
                //使用身份證為預設密碼，若無設定密碼
                Password = idNumber;
            }
            // 將服務項目轉成字串
            String serviceItemsString = arrayToString(Service_Items);

            // 將可服務時間轉成字串
            String serviceabilityTimesString = arrayToString(Serviceability_Times);

            //設定參數，存入資料庫
            BasicInformationEntity v = new BasicInformationEntity();
            v.setIdNumber(idNumber);
            v.setName(Name);
            v.setNationality(Nationality);
            v.setDetail_Nationality(Detail_Nationality);
            v.setOther_Detail_Nationality(Other_Detail_Nationality);
            v.setRepublic(S_Republic);
            v.setBirthday(Birthday);
            v.setSex(Sex);
            v.setAboriginal(S_Aboriginal);
            v.setMarriage(Marriage);
            v.setService_Items(serviceItemsString);
            v.setSpecialty(Specialty);
            v.setSpecialty2(Specialty2);
            v.setSpecialty3(Specialty3);
            v.setPassword(Password);
            v.setDisaster_Relief(S_Disaster_Relief);
            v.setOccupation(Occupation);
            v.setEducational_Background(Educational_Background);
            v.setE_mail(E_mail);
            v.setAddress_Residence_Postal_Code(Address_Residence_Postal_Code);
            v.setAddress_Residence_City(Address_Residence_City);
            v.setAddress_Residence_Area(Address_Residence_Area);
            v.setAddress_Residence_Village(Address_Residence_Village);
            v.setAddress_Residence_Neighborhood(Address_Residence_Neighborhood);
            v.setAddress_Residence_Road_No(Address_Residence_Road_No);
            v.setAddress_Mailing(Address_Mailing);
            v.setContact_Phone(Contact_Phone);
            v.setPhone(Phone);
            v.setContact_Phone2(Contact_Phone2);
            v.setPhone2(Phone2);
            v.setSelf_Introduction(Self_Introduction);
            v.setThe_Earliest_starting_Date_Service(The_Earliest_starting_Date_Service);
            v.setRemark(Remark);
            v.setIncorporation(Incorporation);
            v.setJoin_Date(Join_Date);
            v.setVolunteer_ID(Volunteer_ID);
            v.setVolunteer_Title(Volunteer_Title);
            v.setVolunteer_Identity(Volunteer_Identity);
            v.setServiceability_Start_Date(Serviceability_Start_Date);
            v.setServiceability_End_Date(Serviceability_End_Date);
            v.setServiceability_Times(serviceabilityTimesString);
            v.setOrgCode(org_Code);
            v.setCreate_time(createTime());
            volunteerService.addVolunteer(v);
            return "search_volunteer";
        }
    }

    /**
     * 將 String array 的內容變成單一字串表示，內容使用逗號分開。
     *
     */
    private String arrayToString(String[] source) {
        String resultStr = "";
        for (int i = 0; i < source.length; i++) {
            resultStr = resultStr + source[i] + ",";
            // 移除最後一個逗號。
            if (i == source.length - 1) {
                resultStr = resultStr.substring(0, resultStr.length() - 1);
            }
        }
        return resultStr;
    }
}
