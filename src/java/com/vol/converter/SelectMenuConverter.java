/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vol.converter;

import com.my.Ejb.RegisterEJBLocal;
import com.my.Ejb.VolunteerBeanLocal;
import com.my.Entity.BasicInformationEntity;
import com.vol.bean.volunteer.create_volunteer;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

/**
 *
 * @author user
 */
@ManagedBean(name = "selectMenuConverter")

public class SelectMenuConverter implements Converter {

    public void SelectMenuConverter() {

    }

    @EJB
    VolunteerBeanLocal volunteerService;
    @EJB
    RegisterEJBLocal registerService;

    private BasicInformationEntity selectedVolunteer;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
         return volunteerService.findVolunteer(value); 
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return value.toString();
    }
}
