/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vol.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * 記錄寫入資料庫時，提供額外的資訊，如建立時間。
 *
 * @author hychen39@gmail.com
 */
public class RecordHelper {

    /**
     * Return the current date as the date of creating the new record.
     *
     * @return
     */
    static public String createTime() {
        String createDateStr = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        createDateStr = sdf.format(Calendar.getInstance().getTime());
        return createDateStr;
    }
}
