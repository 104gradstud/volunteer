/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vol.validator;

import com.my.Ejb.VolunteerBeanLocal;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author user
 */
@Named(value = "idNumberCheck")
@RequestScoped
public class idNumberCheck{
    @EJB
    VolunteerBeanLocal volunteerService;
    
    private String checkIdMsg;

    public String getCheckIdMsg() {
        return checkIdMsg;
    }

    public void setCheckIdMsg(String checkIdMsg) {
        this.checkIdMsg = checkIdMsg;
    }

    public void idCheckValidate(FacesContext context, UIComponent component, Object value) 
            throws ValidatorException {
        if (volunteerService.countDuplicateIdnumber(value.toString()) > 0) {
            String msg = "身分證重複!";
            setCheckIdMsg(msg);
            ((UIInput)component).setValid(false);
        } else {
            setCheckIdMsg("");
            ((UIInput)component).setValid(true);
        }
    }
}
