/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vol.validator;

import com.my.Ejb.OrgEJBLocal;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author user
 */
@Named(value = "orgNameCheck")
@RequestScoped
public class orgNameCheck{
    @EJB
    OrgEJBLocal orgService;
    
    private String checkMsg;

    public String getCheckMsg() {
        return checkMsg;
    }

    public void setCheckMsg(String checkMsg) {
        this.checkMsg = checkMsg;
    }

    public void checkValidate(FacesContext context, UIComponent component, Object value) 
            throws ValidatorException {
        if (orgService.countDuplicateOrgName(value.toString()) > 0) {
            String msg = "組織單位名稱重複!";
            setCheckMsg(msg);
            ((UIInput)component).setValid(false);
        } else {
            setCheckMsg("");
            ((UIInput)component).setValid(true);
        }
    }
}
